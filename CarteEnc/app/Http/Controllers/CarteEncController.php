<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CarteEncController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $cartesEtudiant=\App\CarteEtudiant::all();
        return view('index',compact('cartesEtudiant'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //



        $carteEtudiant = new \App\CarteEtudiant ;
        $carteEtudiant->nomEtudiant=$request->get('nomEtudiantFormulaire');
        $carteEtudiant->email=$request->get('email');
        $carteEtudiant->numeroTelephone=$request->get('numeroTelephoneFormulaire');

        $request->validate([
            'nomFichierCV' => 'required|file|max:8192'
        ]);
        $nomFichierAttache = time().request()->nomFichierCV->getClientOriginalName();
        $request->nomFichierCV->storeAs('fichiers',$nomFichierAttache);

        $date=date_create($request->get('dateEntreeENC'));
        $format = date_format($date,'Y-m-d');
        $carteEtudiant->dateEntreeENC = strtotime($format);

        $carteEtudiant->section = $request->get('section');
        $carteEtudiant->nomFichierCV = $nomFichierAttache;

        $carteEtudiant->save();

        return redirect('demandeCarte')->with('success','La nouvelle demande a ete enregistree');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $carteEtudiant=\App\CarteEtudiant::find($id);
        return view('edit',compact('carteEtudiant','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $carteEtudiant = \App\CarteEtudiant::find($id);
        $carteEtudiant->nomEtudiant=$request->get('nom');
        $carteEtudiant->email=$request->get('email');
        $carteEtudiant->numeroTelephone=$request->get('number');
        $carteEtudiant->section = $request->get('section');
        $carteEtudiant->save();
        return redirect('demandeCarte');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $carteEtudiant = \App\CarteEtudiant::find($id);
        $carteEtudiant->delete();
        return redirect('demandeCarte')->with('success','La demande a bien ete supprimée');
    }
}
