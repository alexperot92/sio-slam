<!-- edit.blade.php -->
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Application de saisie de carte étudiant pour ENC  </title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
  </head>
  <body>
    <div class="container">
      <h2>Modification de la demande</h2><br  />
        <form method="post" action="{{action('CarteEncController@update', $id)}}">
        @csrf
        <input name="_method" type="hidden" value="PATCH">
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="nom">Nom:</label>
            <input type="text" class="form-control" name="nom" value="{{$carteEtudiant->nomEtudiant}}">
          </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="email">Email</label>
              <input type="text" class="form-control" name="email" value="{{$carteEtudiant->email}}">
            </div>
          </div>
        <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="number">Numero téléphone:</label>
              <input type="text" class="form-control" name="number" value="{{$carteEtudiant->numeroTelephone}}">
            </div>
          </div>
          <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <strong>Sélectionnez votre fichier</strong>*
              <input type="file" name="nomFichierCV">
            </div>
          </div>
          <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="section">Section :</label>
              <select name="section">
                <option value="SIO SLAM">SIO SLAM</option>
                <option value="SIO SISR">SIO SISR</option>
                <option value="AM">BTS AM</option>
              </select>
            </div>
          </div>
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4" style="margin-top:60px">
            <button type="submit" class="btn btn-success" style="margin-left:38px">Update</button>
          </div>
        </div>
      </form>
    </div>
  </body>
</html>
