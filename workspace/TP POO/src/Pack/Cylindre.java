package Pack;

public class Cylindre {
	//Déclaration des attributs de la classe
	static private double rayon;
	static private double hauteur;
	
	
	//Déclaration du constructeur
	public Cylindre(double H, double R){
		hauteur = H;
		rayon = R;
	}
	
	//Surcharger le constructeur
	public Cylindre(double H){
		hauteur = H;
		rayon = 100;
	}
	//ReSurcharger le constructeur
		public Cylindre(){
			hauteur = 1;
			rayon = 1;
		}
	//methode accesseur
	public static double Getrayon(){
		return rayon;
	}
	public static double Gethauteur(){
		return hauteur;
	}
	public static void Setrayon(double Nouveaurayon){
		rayon = Nouveaurayon;
	}
	public static void Sethauteur(double Nouveauhauteur){
		hauteur = Nouveauhauteur;
	}
	
	
	
	
	
	
	
	
	//Methode qui permet de calculer le volume d'un cylindre
	static public double calcule()
	{
		return 3.1415*rayon*rayon*hauteur;
	}
	
	//Methode qui permet de calculer la surface d'un cylindre
	static public void Surface (){
		double surf;
		surf=3.1415*2*rayon;
		System.out.println("La surface de votre cylindre est de "+surf);
	}
}