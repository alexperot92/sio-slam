package Pack;

import Pack.Saisie;

public class Exo4 {

	public static void main(String[] args) {
		//Déclaration des variables isolées
				int NL,NC,Long,Hauteur,x,y,L,C,Nb;
				//Déclaration des matrices
				int Mat[][];
				//Saisir le nombre de ligne et de colonne
				NL = Saisie.lire_int("Quel est le nombre de ligne ?");
				NC = Saisie.lire_int("Quel est le nombre de colonne ?");
				//Traitement
				Mat = new int [NL][NC];
				
				//Remplissage de la matrice
				Nb=1;
				for(L=0;L<NL;L++){
					for(C=0;C<NC;C++){
						Mat[L][C]=Nb;
						
					}
				}
				Long = Saisie.lire_int("Quel est la longueur du rectangle ?");
				Hauteur = Saisie.lire_int("Quel est la hauteur du rectangle ?");
				x = Saisie.lire_int("Quel sont les coordonnées ?");
				y = Saisie.lire_int("Quel sont les coordonnées ?");
				L=x-1;
				while(L<=x+Hauteur-2){
					C=y-1;
					while(C<=y+Long-2){
						Mat[L][C]=0;
						C++;
					}
					L++;
				}
				for(L=0;L<NL;L++){
					for(C=0;C<NC;C++){
						System.out.print(Mat[L][C]+"  ");
					}
					System.out.println();
				}
	}

}
