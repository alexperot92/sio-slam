package Packages;

import Packages.Saisie;

public class CreationMatrices {

	public static void main(String[] args) {
		//D�claration des variables isol�es
		int NL, NC, Nb, i, y;
		//D�claration des matrices
		int Mat[][];
		//Saisir le nombre de ligne et de colonne
		NL = Saisie.lire_int("Veuillez saisir votre nombre de ligne");
		NC = Saisie.lire_int("Veuillez saisir le nombre de colonne");
		//Traitement
		Mat = new int [NL][NC];
		Nb=0;
		for(i=0;i<NL;i++){
			for(y=0;y<NC;y++){
				Nb = Nb+1;
				Mat[i][y] = Nb;
			}
		}
		//Affichage
		for(i=0;i<NL;i++){
			for(y=0;y<NC;y++){
				System.out.print(Mat[i][y]+" ");
			}
			System.out.println("");
		}

	}

}
