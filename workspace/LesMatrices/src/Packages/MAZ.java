package Packages;

public class MAZ {
	
	public static void main(String[] args) {
		//déclaration des variable
		int N, I, L, C;
		int MAT[][];
		//saisie de N
		N =Saisie.lire_int("Veuillez saisir l'ordre de la matrice");
		while(N <= 2 ){
			N =Saisie.lire_int("Veuillez resaisir l'ordre de la matrice");
		}
		//instanciation de la matrice
		MAT = new int[N][N];
		//remplissage de la matrice avec des 9
		for(L=0;L<N;L++){
			for(C=0;C<N;C++){
				MAT[L][C] = 9;
			}
		}
		
		// MAZ de la diagonale principale*
		I = 0;
		while(I<N){
			MAT[I][I] = 0;
			I++;
		}
		
		
		
		
		//MAZ de la seconde diagonale
		I = N-1;
		for(L=0;L<N;L++){
			MAT[L][I] = 0;
			I--;
		}
		
		
		//Affichage du resultat
				for(L=0;L<N;L++){
					for(C=0;C<N;C++){
						System.out.print(MAT[L][C]+" , ");
					}
					System.out.println();
				}
	}
}
