package Packages;
import java.util.*;
public class GestionMatrice {

	public static void main(String[] args) {
		//D�claration des matrices
		int Mat[][] ;
		//D�claration des variables isol�es
		int N,L, C, Choix, NBZero, SommeCol, SommeLigne, Max;
		// Saisie de l'ordre de la matrice et controle de saisie
		N = Saisie.lire_int("Veuillez saisir l'ordre de votre matrice");
		while(N <= 2){
			N = Saisie.lire_int("Saisie incorrecte, veuillez recommencer");
		}
		Mat = new int[N][N];
		
		//Remplire la matrice
		for(L=0; L<N; L++){
			for(C=0; C<N; C++){
				Mat[L][C] = Saisie.lire_int("Veuillez saisir matrice");
			}
		}
		
		//Affichage de la matrice
		L = 0;
		while(L < N){
			C = 0;
			while(C < N){
				System.out.println(Mat[L][C]);
				C = C+1;
			}
			L = L+1;
		}
		
		//affichage du menu
		System.out.println("Voici le menu proposer");
		System.out.println(" Taper 1 pour connaitre le nombre d'�l�ments nul");
		System.out.println(" Taper 2 pour faire la somme colone par colone");
		System.out.println(" Taper 3 pour faire la somme ligne par ligne");
		System.out.println(" Taper 4 pour connaitre l'�l�ments maximum");
		
		
		// Choix d'action par l'utilisateur
		Choix = Saisie.lire_int("Veuillez saisir votre Choix");
		
		
		//Traitement
		switch(Choix){
			case 1 : NBZero = 0;
					 for(L=0; L<N; L++){
						 for(C=0; C<N; C++){
							 if(Mat[L][C] == 0){ NBZero = NBZero+1; }
						 }
					 }
					 System.out.println("La matrice compte "+NBZero+" z�ros");
					 break;
			case 2 : SommeCol = 0;
					 for(C=0; L<N; L++){
						 for(L=0; C<N; C++){
							 SommeCol = SommeCol + Mat[L][C];
						 }
					 }
					 System.out.println("La somme des �l�ments colone par colone vaut "+SommeCol);
					 break;
			case 3 : SommeLigne = 0;
					 for(L=0; L<N; L++){
						 for(C=0; C<N; C++){
							 SommeLigne = SommeLigne + Mat[L][C];
						 }
					 }
					 System.out.println("La somme des �l�ments ligne par ligne vaut "+SommeLigne);
					 break;
			case 4 : Max = Mat[1][1];
					 for(L=0; L<N; L++){
						 for(C=0; C<N; C++){
							 if(Mat[L][C] > Max){Max = Mat[L][C];}
						 }
					 }
					 System.out.println("L'�l�ment maximum de la matrice vaut "+Max);
					 break;
			default : System.out.println("Saisie incorrecte donc pas de r�sultat");
		System.out.println(" Fermeture de l'application ");
					 
		}
	}

}
