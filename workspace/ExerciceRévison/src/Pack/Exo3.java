package Pack;

public class Exo3 {

	public static void main(String[] args) {
		int T1[]={-1,2,0,5,3,4,0,-5};
		int T2[]={1,2,3,4,5,6,7,8};
		int i, gauche, droite;
		boolean place;
		gauche = 0;
		droite = 7;
		place = true;
		for(i=0; i<8;i++){
			if(T1[i] != 0){
				if(place == true){
					T2[gauche] = T1[i];
					gauche++;
					place = false;
				}
				else{
					T2[droite] = T1[i];
					droite--;
					place = true;
				}
			}
		}
		for(i=0;i<8;i++){
			System.out.print(T2[i]+"  ");
		}
	}
}
