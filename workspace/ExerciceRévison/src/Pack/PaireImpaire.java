package Pack;

import Pack.Saisie;

public class PaireImpaire {

	public static void main(String[] args) {
		int a;
		boolean test;
		a = Saisie.lire_int("Veuillez saisir un nombre");
		test = estPaire(a);
		if(test==true){
			System.out.println("C'est paire");
		}
		else{
			System.out.println("C'est impaire");
		}
	}
	public static boolean estPaire(int a){
		boolean test;
		int reste;
		reste = a%2;
		if(reste==0){
			test = true;
		}
		else{
			test = false;
		}
		return test;
	}
}
