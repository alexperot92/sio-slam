package Pack;

public class Compte {
	private String NomCompte;
	private int NumCompte;
	private double SoldeCompte;
	private int decouvert;
	public Compte(){
		NomCompte=Saisie.lire_String("Saisissez le Nom du compte");
		NumCompte=Saisie.lire_int("Saisissez le Num du compte");
		SoldeCompte=Saisie.lire_double("Saisissez le solde de votre compte");
		decouvert=Saisie.lire_int("Saisissez votre decouvert");
	}
	public String getNomCompte(){
		return NomCompte;
	}
	public int getNumCompte(){
		return NumCompte;
	}
	public double getSoldeCompte(){
		return SoldeCompte;
	}
	public int getdecouvert(){
		return decouvert;
	}
	public void setNomCompte(String NomC){
		NomCompte=NomC;
	}
	public void setNumCompte(int NumC){
		NumCompte=NumC;
	}
	public void setSoldeCompte(double SoldeC){
		SoldeCompte=SoldeC;
	}
	public void setdecouvert(int decouvertC){
		decouvert=decouvertC;
	}
	public void affich(){
		System.out.println("Voila la liste des compte :"+NomCompte);
	}
	//M�thode constructeur
	public Compte(String Nom, int NumS, double Solde, int decouvertS){
		NomCompte=Nom;
		NumCompte=NumS;
		SoldeCompte=Solde;
		decouvert=decouvertS;
	}
	public void NomCompte(){
		System.out.println("Compte : "+NomCompte);
	}
	public void consulte(){
		System.out.println("Le solde de votre compte est "+SoldeCompte+" euros");
	}
	public void depot(double depot){
		SoldeCompte=SoldeCompte+depot;
		System.out.println("Votre depot a �tais r�ussie avec succ�s");
	}
	public void retrait(double retrait){
		if((SoldeCompte-retrait) < -decouvert){
			System.out.println("Vous n'avez pas les fonds suffisants sur votre compte pour effectuer un retrait");
		}
		else{
			SoldeCompte=SoldeCompte-retrait;
			System.out.println("Votre retrait a �t� r�ussi avec succ�s");
		}
	}
	
}
