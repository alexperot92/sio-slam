package Package;

public class Exo3 {

	public static void main(String[] args) 
	{
		int Tab[];
		int N, I;
		boolean Test;
		
		//Cr�er et remplir le tableau
		N=Saisie.lire_int("Veuillez saisir les nombres d'�l�ments de votre vecteur");
		
		Tab= new int[N];
		
		System.out.println("Remplissez le tableau :");
		
		
		
		for (I=0; I < N; I++) 
		{
			Tab[I]=Saisie.lire_int("Quelle est la valeur de la case de rang "+ I+ " ?");
		}
		
		Test=VTab(Tab, N);
		
		if (Test==true)
		{
			System.out.println("Le tableau est tri�");
			
		}
		else System.out.println("Le tableau n'est pas tri�");


	}
	
	public static boolean VTab(int Tab[], int N)
	{
		int I;
		boolean Res;
		
		Res=true;
		I=0;
		while ((I<N-1)&&(Res==true))
		{
			if (Tab[I]>Tab[I+1])
			{
				Res=false;
			}
			else I=I+1;
		}
		
		return Res;
	}

}
