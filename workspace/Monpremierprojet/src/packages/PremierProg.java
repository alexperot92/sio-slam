package packages;

public class PremierProg {

	public static void main(String[] args) {
		//D�clarer des variables
		int Age;
		double SoldeCompte;
		String Nom;
		
		//Saisie des valeurs par l'utilisateur
		Age = Saisie.lire_int("Veuillez saisir votre age");
		SoldeCompte = Saisie.lire_double("Veuillez saisir le solde de votre compte");
		Nom = Saisie.lire_String("Veuillez saisir votre nom");
		
		//Affichage des r�sultats
		System.out.println("Votre nom est "+Nom);
		System.out.println("Vous avez "+Age+" ans");
		System.out.println("Votre solde est de "+SoldeCompte+" �");
	}

}
