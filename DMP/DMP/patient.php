<?php
session_start();
$identifiant= $_SESSION['ID'];
try
{
  $bdd = new PDO("mysql:host=localhost;dbname=DMP2", 'root', '');
}catch(Exception $e)
{
  die('Erreur : '. $e->getMessage());
}
$requete=$bdd->query("SELECT numSS,nomP,prenomP, numRPPS  FROM PATIENT where numSS='$identifiant' ");
$reponse= $requete->fetchAll();
foreach($reponse as $donnees){
  $_SESSION['nom']=$donnees['nomP'];
}
?>


<!DOCTYPE html>
<html>
<head>
	<title></title>
  <meta charset="utf-8">
	<link rel="stylesheet" href="css/patientcss.css">
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
  <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>


	<!--barre de navigation-->
 <nav class="navbar navbar-light  bg-light">
      <div class="navbar-nav">
        <a class="nav-item nav-link active" style="color:#007bff;" href="Patient.php"> <h2>DMP</h2>  </a>

      </div>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
          <a class="nav-item nav-link active font-weight-bold" href="#haut">Patient</a>
          <a class="nav-item nav-link active" href="SavoirPlus.html">Découvrir le DMP</a>
            <a class="nav-item nav-link active" href="Patient.php">Mon DMP</a>
          <a class="nav-item nav-link active" href="SignP.php">Créer son DMP</a>
           <hr size="0,5" color="black">
          <a class="nav-item nav-link active font-weight-bold" href="#folio">Professionnel</a>
          <a class="nav-item nav-link active" href="#">Découvrir le DMP</a>
          <a class="nav-item nav-link active" href="#">créer son DMP</a>
        </div>
      </div>
    </nav>
<!--Fin Barre de Navigation-->
<div class="ban">
 <img src="img/medecin.jpg"  class="img-fluid" alt="Responsive image">
 <div class="textover container" style="position: relative; margin-top: -30%; color: white; background-color:#007bff">
   <center><h1>BIENVENUE</h1>
   <h2><?php echo "MONSIEURS ".$_SESSION['nom']; ?> !</h2></center>
 </div>
</div>
<!--Dernier RDV MEDICAUX-->
<br>
<div id="competence" class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: 25%;">
    <h2 style="text-align: center; color:#007bff ;">Vos derniers rendez-vous</h2>
    <div class="container">
      <hr size="0,5" style="width:20%" color="#2C77B9">
    </div>

    <i class="col-md-12 col-sm-12 col-xs-12 icon fa fa-info-circle fa-3x" aria-hidden="true"></i>
</div>
<div class="container">
  <table class="table table-hover ">
    <?php
    $requete4=$bdd->query("SELECT m.nomM , c.diagnostic, c.dateSS, m.specialite FROM medecin m INNER JOIN consulte c ON m.numRPPS=c.numRPPS WHERE c.numSS=$identifiant " );
    $reponse4= $requete4->fetchAll();
    echo "
    <thead>
      <tr>

        <th scope=\"col\">Médecin</th>
        <th scope=\"col\">Spécialité</th>
        <th scope=\"col\">Diagnostic</th>
        <th scope=\"col\">date</th>
      </tr>
    </thead>";
    foreach ($reponse4 as $donnees) {
   echo "<tbody>
      <tr>
        <td>".$donnees['nomM']."</td>
        <td>".$donnees['specialite']."</td>
        <td>".$donnees['diagnostic']."</td>
        <td>".$donnees['dateSS']."</td>
      </tr>" ;
}
    ?>

  </table>
</div>
<!--Fin Dernier RDV MEDICAUX-->




<!--fin Ordonnances-->
<script src="http://momentjs.com/downloads/moment-with-locales.js"></script>
<script src="http://momentjs.com/downloads/moment-timezone-with-data.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
