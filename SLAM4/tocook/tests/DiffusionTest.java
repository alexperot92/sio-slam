import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class DiffusionTest {

    Date uneDate ;
    CategorieCSA uneCateg ;
    Emission uneEmi ;
    Programme unProg ;
    Diffusion uneDiff ;

    @BeforeEach
    public void setUp() throws Exception {

        uneDate = DateUtils.asDate(LocalDate.parse("2019-06-01"));
        uneCateg = new CategorieCSA("TP", "Tout public");
        uneEmi = new Emission(212234, "Chefs Saison 1");
        unProg = new Programme(1, "Épisode 1", 47, uneCateg, uneEmi);
        uneDiff = new Diffusion(8, uneDate, "13:30:00", false, unProg);
    }
    @Test
    public void testToXML() {
        String resultat = "<diffusion><horaire>13:30:00</horaire>" +
					"<duree>47</duree><emission>Chefs Saison 1</emission>" +
					"<programme>Épisode 1</programme><csa>Tout public</csa></diffusion>";

        // Début question 4
        Assertions.assertEquals(resultat,uneDiff.toXML(),"Erreur");
    }

    @Test
    public void testDiffusionsDirect() {
                // Début question 6
        CategorieCSA newCATECSA = new CategorieCSA("-10","Déconseiller pour les moins de 10 ans");
        Emission newEmission = new Emission(123122,"A table ! Saison 2");
        Programme newProg = new Programme(1,"Episode 1",90,newCATECSA,newEmission);
        Diffusion newDiff = new Diffusion(234234,uneDate,"10h29",true,newProg);
        ArrayList<Diffusion> lesDiffusion = new ArrayList<>();
        lesDiffusion.add(newDiff);
        lesDiffusion.add(uneDiff);
        Application uneApplication = new Application(lesDiffusion);
        assertThat(uneApplication.getLesDiffusions(),hasItems(newDiff,uneDiff));
        assertThat(uneApplication.getDiffusionsEnDirect(uneDate),hasItem(newDiff));
        assertThat(uneApplication.getDiffusionsEnDirect(uneDate),not(hasItems(uneDiff)));
    }
    }
