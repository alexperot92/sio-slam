import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Application {
    private ArrayList<Diffusion> lesDiffusions ;

    // Question 6: ajouts constructeurs et getter et setter

    public Application(ArrayList<Diffusion> lesDiffusions) {
        this.lesDiffusions = lesDiffusions;
    }

    public ArrayList<Diffusion> getLesDiffusions() {
        return lesDiffusions;
    }

    public void setLesDiffusions(ArrayList<Diffusion> lesDiffusions) {
        this.lesDiffusions = lesDiffusions;
    }

    public String diffusionsXML(Date uneDate) {
        String xml;
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-mm-dd");
        xml = "<?xml version = \"1.0\" encoding=\"UTF-8\" ?>";
        xml += "<programmation>";
        xml += "<jour>" + sf.format(uneDate) + "</jour>";

        // A compléter sur votre copie
        xml += "<diffusion>";
        for(Diffusion laDiffusion : lesDiffusions)
        {
            if(laDiffusion.getLeJour().equals(uneDate))
                xml += laDiffusion.toXML();
        }
        xml += "</diffusion>";
        xml += "</programmation>" ;
        return xml;

    }

    public ArrayList<Diffusion> getDiffusionsEnDirect(Date uneDate) {

        // Début Question 5
        ArrayList<Diffusion> lesdiffusionsEnDirect = new ArrayList<>();
        for(Diffusion laDiffusion : lesDiffusions)
            if(laDiffusion.getLeJour().equals(uneDate) && laDiffusion.isDirect())
                lesdiffusionsEnDirect.add(laDiffusion);
        // Fin Question 5
        return lesdiffusionsEnDirect;
    }
    }
