import java.text.SimpleDateFormat;
import java.util.Date;

public class Diffusion {
    private int id;
    private Date leJour;   //date de diffusion
    private String horaire;
    private boolean direct;
    private Programme leProgramme;

    public Diffusion(int id, Date leJour, String horaire, boolean direct, Programme leProgramme) {
        this.id = id;
        this.leJour = leJour;
        this.horaire = horaire;
        this.direct = direct;
        this.leProgramme = leProgramme;
    }

    public Date getLeJour() {
        return leJour;
    }

    // permet  de modifier la date de diffusion en s’assurant que la nouvelle date soit postérieure
    // à la date du jour. Retourne « true » si la modification a été effectuée, « false » sinon.

    public boolean setLeJour(Date dateDiffusion) {
        // Ajouts question 2
        Date dateDuJour = new Date();
        if(dateDiffusion.after(dateDuJour)) this.leJour = dateDiffusion;
        else return false;
        return true;
        // Fin Ajouts question 2
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHoraire() {
        return horaire;
    }

    public void setHoraire(String horaire) {
        this.horaire = horaire;
    }

    public boolean isDirect() {
        return direct;
    }

    public void setDirect(boolean direct) {
        this.direct = direct;
    }

    public Programme getLeProgramme() {
        return leProgramme;
    }

    public void setLeProgramme(Programme leProgramme) {
        this.leProgramme = leProgramme;
    }

    public String toXML(){
        String xml = "" ;
        xml+= "<diffusion>" ;
        // Début de la question 3 à ajouter
        xml+= "<horaire>"+getHoraire()+"</horaire>" ;
        xml+= "<duree>"+getLeProgramme().getDuree()+"</duree>" ;
        xml+= "<emission>"+getLeProgramme().getlEmission().getTitre()+"</emission>" ;
        xml+= "<programme>"+leProgramme.getTitre()+"</programme>" ;
        xml+= "<csa>"+leProgramme.getLaCategorieCSA().getLibelle()+"</csa>" ;
        xml += "</diffusion>";
        return xml;
       // Fin du code de la question 3 à ajouter
    }

}
