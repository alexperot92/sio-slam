import org.joda.time.DateTime ;

public abstract class Expertise {
	
	private String codeDossier ;
	
	 private DateTime dateHeureRDV;
	 private String lieuRDV ;
	 private String adresse ;
	 private String immatriculation ;
	 private String marque ;
	 private String modele ;
	 private Indisponibilite uneIndispo;
	 private int etatExpertise ;

	public Expertise(String codeDossier, DateTime uneDateHeure, String unLieu,
					 String uneAdresse, String uneImmat, String uneMarque, String unModele, int etatExpertise)
	{

		this.codeDossier = codeDossier ;
		this.dateHeureRDV = uneDateHeure ;
		this.adresse = uneAdresse ;
		this.immatriculation = uneImmat ;
		this.marque = uneMarque ;
		this.modele = unModele ;
		//this.uneIndispo = new Indisponibilite("Inconnu",false) ;
		this.etatExpertise = etatExpertise ;
		this.uneIndispo = null ;
	}

	public String getCodeDossier() {
		return codeDossier;
	}

	public void setCodeDossier(String codeDossier) {
		this.codeDossier = codeDossier;
	}

	public DateTime getDateHeureRDV() {
		return dateHeureRDV;
	}

	public void setDateHeureRDV(DateTime dateHeureRDV) {
		this.dateHeureRDV = dateHeureRDV;
	}

	public String getLieuRDV() {
		return lieuRDV;
	}

	public void setLieuRDV(String lieuRDV) {
		this.lieuRDV = lieuRDV;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getImmatriculation() {
		return immatriculation;
	}

	public void setImmatriculation(String immatriculation) {
		this.immatriculation = immatriculation;
	}

	public String getMarque() {
		return marque;
	}

	public void setMarque(String marque) {
		this.marque = marque;
	}

	public String getModele() {
		return modele;
	}

	public void setModele(String modele) {
		this.modele = modele;
	}

	public Indisponibilite getUneIndispo() {
		return uneIndispo;
	}

	public void setUneIndispo(Indisponibilite uneIndispo) {
		this.uneIndispo = uneIndispo;
	}

	public int getEtatExpertise() {
		return etatExpertise;
	}

	public void setEtatExpertise(int etatExpertise) {
		this.etatExpertise = etatExpertise;
	}

	@Override
	public String toString() {
		return "Expertise [codeDossier=" + codeDossier + ", dateHeureRDV=" + dateHeureRDV + ", lieuRDV=" + lieuRDV
				+ ", adresse=" + adresse + ", immatriculation=" + immatriculation + ", marque=" + marque + ", modele="
				+ modele + ", uneIndispo=" + uneIndispo  + "]";
	}

	//Constructeur permettant de valoriser les attributs d�un objet

	 

	 //M�thode qui permet de cr�er l'indisponibilit� de l�expertise qui n'a pas pu avoir lieu
	 public void creerIndisponibilite(String unMotif, boolean clientResponsable)
	 { 
		 this.uneIndispo = new Indisponibilite(unMotif,clientResponsable) ;
		 
	 }
	 //Obtient l'objet de type Indisponibilite si l�expertise n�a pas pu avoir lieu
	 //<returns>objet de type Indisponibilite ou valeur null si expertise � r�aliser ou r�alis�e</returns>
	 public Indisponibilite getIndisponibilite() { 
		 return this.uneIndispo ;
		 
	 }

	

}
