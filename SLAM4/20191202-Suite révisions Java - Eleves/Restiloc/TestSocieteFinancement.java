package restiloc;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;

import org.joda.time.DateTime;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestSocieteFinancement {

	SocieteFinancement uneSociete ;

	@BeforeEach
	public void setUp() throws Exception {
		
		uneSociete = new SocieteFinancement("AA","AA") ;
			
		
		
	}


	@Test
	public void testNbIndispo() {
		uneSociete.ajouterExpertisePool("AA",DateTime.now() , "MA",
				"Carnot", "AA-001-AA", "DACIA", "MCV", 1,"Aucun",false);
		
		Assertions.assertEquals(uneSociete.calculeNbIndisponibilites("Piscine"),0,"Pas bon") ;
	
		uneSociete.ajouterExpertisePool("AA",DateTime.now() , "MA",
				"Carnot", "AA-001-AA", "DACIA", "MCV", 3,"Aucun",false);
		
		Assertions.assertEquals(uneSociete.calculeNbIndisponibilites("Piscine"),0,"Pas bon") ;
		
		uneSociete.ajouterExpertisePool("AA",DateTime.now() , "MA",
				"Carnot", "AA-001-AA", "DACIA", "MCV", 3,"Piscine",false);
		
		Assertions.assertEquals(uneSociete.calculeNbIndisponibilites("Piscine"),1,"Pas bon") ;
		
	}
	
	@Test
	public void testAvecIndisponibiliteNulle() {
		uneSociete.ajouterExpertisePool("AA",DateTime.now() , "MA",
				"Carnot", "AA-001-AA", "DACIA", "MCV", 1);
		
		Assertions.assertEquals(uneSociete.calculeNbIndisponibilites("Piscine"),0,"Pas bon") ;
	
		
	}

	@Test
	public void testLesIndispos() {
		uneSociete.ajouterExpertisePool("AA",DateTime.now() , "MA",
				"Carnot", "AA-001-AA", "DACIA", "MCV", 1,"Aucun",false);
		
		
		assertThat(uneSociete.lesExpertisesIndispos(), hasSize(0));
		
		uneSociete.ajouterExpertisePool("AA",DateTime.now() , "MA",
				"Carnot", "AA-001-AA", "DACIA", "MCV", 3,"Aucun",false);
		
		
		assertThat(uneSociete.lesExpertisesIndispos(), hasSize(1));
		
		
	}
}
