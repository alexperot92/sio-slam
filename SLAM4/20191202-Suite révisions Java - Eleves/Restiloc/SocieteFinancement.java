import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime ;

public class SocieteFinancement {
	
	private String code;
	 private String nom;
	 private List<Expertise> lesExpertises;
	 // Constructeur qui initialise les attributs priv�s et instancie la collection lesExpertises
	 public SocieteFinancement (String codeSL, String nomSL) {
		 this.code = codeSL ;
		 this.nom = nomSL ;
		 this.lesExpertises = new ArrayList<Expertise>();
		 
	 }

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public List<Expertise> getLesExpertises() {
		return lesExpertises;
	}

	public void setLesExpertises(List<Expertise> lesExpertises) {
		this.lesExpertises = lesExpertises;
	}

	public void ajouterExpertiseClient (String dossier, DateTime dateHeure, String lieu, String adresse,
										String immat, String marque, String modele, String unContact, String unTel, String unMail) {
		 
	 }

	


	 // M�thode qui permet d'ajouter une nouvelle expertise de type pool garage
	 public void ajouterExpertisePool (String dossier, DateTime dateHeure, String lieu,	 String adresse, String immat, String marque, String modele, 
			 int etatExpertise)
	 {
	// A completer sur votre copie
		 
	 }

	 // M�thode qui retourne la liste des expertises indisponibles
	 public List<Expertise> lesExpertisesIndispos ()
	 {
	// � compl�ter sur votre copie
		 
		 
		 
	 }
	 // M�thode qui retourne le nombre d�expertises indisponibles correspondant au motif leMotif
	 public int calculeNbIndisponibilites(String leMotif)
	 {
	// � compl�ter sur votre copie
		 
		 
	 }

}
