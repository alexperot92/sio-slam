<?php
class Adresse {
protected $ville;
protected $pays;

public function setVille($ville) {
  $this->ville = $ville;
  }
public function getVille() {
  return $this->ville; }

public function setPays($pays) {
  $this->pays = $pays;
  }
public function getPays() {
  return $this-> pays;
  }
}

class Personne {
protected $nom;
protected $adresse;

function __construct($nom){
    $this->nom=$nom;
    $this->adresse=new Adresse();
  }
public function setNom($nom) {
    $this->nom = $nom;
  }
public function getNom() {
  return $this->nom;
  }
public function setAdresse($adresse) {
    $this->adresse = $adresse;
  }
public function getAdresse() {
  return $this->adresse;
  }

public function affichetout(){
  echo "Le nom est: ".$this->getNom();
  echo "La ville est: ".$this->getAdresse()->getVille();
}
}
$unepersonne = new Personne("Super Eleve<br>");
$unepersonne->getAdresse()->setVille("Neuilly<br>");
$unepersonne->affichetout();

?>
