<!doctype html>
<html lang="fr">
<head>
<meta charset="utf-8">
<title>Rectangle</title>

</head>
<body>
<?php
// Inclusion de la classe Rectangle:
require('EXO2_Rectangle.php');

// Definition longueur et largeur:
$longueur = 160;
$largeur = 75;


// Message bienvenue:
echo "<h2>Etude de rectangle de longueur $longueur et largeur $largeur</h2>";

// Création de rectangle:
$rectangle = new Rectangle($longueur,$largeur);


// Transformation au carré
$rectangle->definitTaille(80,80);

// Impression aire.
$aire = $rectangle->recupereSurface();
echo "L'aire est de $aire<br>";

// Recupere le perimetre.
$perimetre = $rectangle->recuperePerimetre();
echo "Le perimetre est de $perimetre<br>";

// Est ce un carre?
echo '<p>Ce rectangle ';

// instruction if à ajouter

if ($carre=$rectangle->estCarre())
{
echo 'est aussi';
} else {
echo " n'est pas";
}
echo ' un carre.</p>';
// Destruction du rectangle:
unset($r);

?>
</body>
</html>
