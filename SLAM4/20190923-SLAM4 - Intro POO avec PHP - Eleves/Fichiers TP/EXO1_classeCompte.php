<?php
class Compte
{
public $montant;

public function virer($valeur, $destination)
{

    $this->montant -= $valeur;
    $destination->montant += $valeur;

}
}


$compteProfesseur = new Compte();
$compteProfesseur->montant = 100 ;

$compteEleve = new Compte();
$compteEleve->montant = 100 ;

$compteEleve->virer(50,$compteProfesseur);

echo "Prof : $compteProfesseur->montant<br>";
echo "Prof : $compteEleve->montant<br>";

// Insérez en dessous le code permettant de créer
// une instance de Compte, nommée compteProfesseur


?>
