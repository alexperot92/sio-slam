<?php
/**
 * Created by PhpStorm.
 * User: aperot
 * Date: 07/10/2019
 * Time: 12 h 04
 */
require "./autoload.php" ;

use PHPUnit\Framework\TestCase;

class NoteTest extends TestCase
{
    public function testNoteTotal(){
        $uneNote = new Note(18,0.5,1);
        $this->assertEquals(9.5,$uneNote->noteTotale(),"Probleme dans le calcul");
}
}
