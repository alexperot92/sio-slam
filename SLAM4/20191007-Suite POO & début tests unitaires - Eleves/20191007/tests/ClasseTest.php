<?php
require "./autoload.php" ;


use PHPUnit\Framework\TestCase;

class ClasseTest extends TestCase
{

    public function testVerifiePassagerClandestin()
    {
        $slamOption = new Option("SIO","BTS",19,"SLAM",16) ;
        $sisrOption = new Option("SIO","BTS",16,"SISR",14) ;

        $eleveRomas = new Eleve("Romas","Lionia",$slamOption) ;
        $eleveThanh = new Eleve("Thanh","Tony",$slamOption) ;
        $eleveDebure = new Eleve("Debure","Lucas",$sisrOption) ;

        $listeEleves = array($eleveRomas,$eleveDebure,$eleveThanh) ;

        $classeSio2Slam = new Classe($slamOption,$listeEleves) ;

        $this->assertEquals(1,count($classeSio2Slam->listePassagersClandestins()), "Erreur calcul passager clandestin") ;

    }
}
