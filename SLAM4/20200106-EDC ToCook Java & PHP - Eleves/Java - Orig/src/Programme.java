public class Programme {
    private int id;
    private String titre;
    private int duree;
    private CategorieCSA laCategorieCSA;
    private Emission lEmission;

    public Programme(int id, String titre, int duree, CategorieCSA laCategorieCSA, Emission lEmission) {
        this.id = id;
        this.titre = titre;
        this.duree = duree;
        this.laCategorieCSA = laCategorieCSA;
        this.lEmission = lEmission;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public int getDuree() {
        return duree;
    }

    public void setDuree(int duree) {
        this.duree = duree;
    }

    public CategorieCSA getLaCategorieCSA() {
        return laCategorieCSA;
    }

    public void setLaCategorieCSA(CategorieCSA laCategorieCSA) {
        this.laCategorieCSA = laCategorieCSA;
    }

    public Emission getlEmission() {
        return lEmission;
    }

    public void setlEmission(Emission lEmission) {
        this.lEmission = lEmission;
    }

    @Override
    public String toString() {
        return "Programme{" +
                "id=" + id +
                ", titre='" + titre + '\'' +
                ", duree=" + duree +
                ", laCategorieCSA=" + laCategorieCSA +
                ", lEmission=" + lEmission +
                '}';
    }

}
