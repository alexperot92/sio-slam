public class Emission {
    private int id;
    private String titre;
    private String titreOriginal;
    private String anneeProduction;
    private int numSaison;
    private Genre sonGenre;


    public Emission(int id, String titre) {
        this.id = id;
        this.titre = titre;
    }

    public Emission(int id, String titre, String titreOriginal, String anneeProduction, int numSaison, Genre sonGenre) {
        this.id = id;
        this.titre = titre;
        this.titreOriginal = titreOriginal;
        this.anneeProduction = anneeProduction;
        this.numSaison = numSaison;
        this.sonGenre = sonGenre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getTitreOriginal() {
        return titreOriginal;
    }

    public void setTitreOriginal(String titreOriginal) {
        this.titreOriginal = titreOriginal;
    }

    public String getAnneeProduction() {
        return anneeProduction;
    }

    public void setAnneeProduction(String anneeProduction) {
        this.anneeProduction = anneeProduction;
    }

    public int getNumSaison() {
        return numSaison;
    }

    public void setNumSaison(int numSaison) {
        this.numSaison = numSaison;
    }

    public Genre getSonGenre() {
        return sonGenre;
    }

    public void setSonGenre(Genre sonGenre) {
        this.sonGenre = sonGenre;
    }


    @Override
    public String toString() {
        return "Emission{" +
                "id=" + id +
                ", titre='" + titre + '\'' +
                ", titreOriginal='" + titreOriginal + '\'' +
                ", anneeProduction='" + anneeProduction + '\'' +
                ", numSaison=" + numSaison +
                ", sonGenre=" + sonGenre +
                '}';
    }

}
