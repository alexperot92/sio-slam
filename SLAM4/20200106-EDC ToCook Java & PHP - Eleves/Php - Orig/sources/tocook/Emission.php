<?php


namespace tocook;


class Emission
{
    private $id;
	private $titre;
	private $titreOriginal;
	private $anneeProduction;
	private $numSaison;
	private $sonGenre;

    /**
     * Emission constructor.
     * @param $id
     * @param $titre
     * @param $titreOriginal
     * @param $anneeProduction
     * @param $numSaison
     * @param $sonGenre
     */
    public function __construct($id, $titre, $titreOriginal="", $anneeProduction=2019, $numSaison=1, $sonGenre="Fiction")
    {
        $this->id = $id;
        $this->titre = $titre;
        $this->titreOriginal = $titreOriginal;
        $this->anneeProduction = $anneeProduction;
        $this->numSaison = $numSaison;
        $this->sonGenre = $sonGenre;
    }


    /**
     * @return mixed
     */
    public function getId() {  return $this->id;   }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitre(){ return $this->titre; }

    /**
     * @param mixed $titre
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;
    }

    /**
     * @return mixed
     */
    public function getTitreOriginal()
    {
        return $this->titreOriginal;
    }

    /**
     * @param mixed $titreOriginal
     */
    public function setTitreOriginal($titreOriginal)
    {
        $this->titreOriginal = $titreOriginal;
    }

    /**
     * @return mixed
     */
    public function getAnneeProduction()
    {
        return $this->anneeProduction;
    }

    /**
     * @param mixed $anneeProduction
     */
    public function setAnneeProduction($anneeProduction)
    {
        $this->anneeProduction = $anneeProduction;
    }

    /**
     * @return mixed
     */
    public function getNumSaison()
    {
        return $this->numSaison;
    }

    /**
     * @param mixed $numSaison
     */
    public function setNumSaison($numSaison)
    {
        $this->numSaison = $numSaison;
    }

    /**
     * @return mixed
     */
    public function getSonGenre()
    {
        return $this->sonGenre;
    }

    /**
     * @param mixed $sonGenre
     */
    public function setSonGenre(Genre $sonGenre)
    {
        $this->sonGenre = $sonGenre;
    }

    public function __toString()
    {
        // TODO: Implement __toString() method.
        return "ID Emission : " . $this->getId() . " Titre : " . $this->getTitre() .
            " Version Originale : " . $this->getTitreOriginal() . " Année : " . $this->getAnneeProduction() . " Saison : " .
            $this->getNumSaison() . " Genre : " . $this->getSonGenre();

    }


}