<?php


namespace tocook;


class Genre
{
    private $code, $libelle ;

    /**
     * Genre constructor.
     * @param $code
     * @param $libelle
     */
    public function __construct($code, $libelle)
    {
        $this->code = $code;
        $this->libelle = $libelle;
    }

    public function __toString()
    {
        // TODO: Implement __toString() method.
        return "Code: ". $this->getCode(). " et Libellé: ". $this->getLibelle() ;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @param mixed $libelle
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
    }


}