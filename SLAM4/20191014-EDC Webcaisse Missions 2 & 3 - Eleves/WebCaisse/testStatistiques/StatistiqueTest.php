<?php

require "autoload.php";
use PHPUnit\Framework\TestCase;

class StatistiqueTest extends TestCase
{
    protected $conso1, $conso2, $mrtoutlemondeConso ;

    protected function setUp() {
        $this->conso1 = new ConsoFidele("Dupond","gérard","robert.btssio@gmail.com", "0615151515","1971-06-15","2018-01-05") ;
        $this->conso2 = new ConsoFidele("Durand","Geraldine","geraldine.durand@gmail.com", "0620253035","1977-05-04","2018-05-05");
        $this->mrtoutlemondeConso = new Conso("Dupond","Gerard","gerard.dupond@gmail.com", "0635353535") ;


    }

    public function testCompareLieuVente()
    {

        $uneVenteMagasin = new VenteMagasin("24091",null,100,"01");
        $this->conso1->addUneVente($uneVenteMagasin);
        $uneVenteECommerce = new VenteEcommerce("464",null, 400,"chez toi", "01");

      //  $uneVenteMagasin = new VenteMagasin("26091",null,300,"01");
        $this->conso2->addUneVente($uneVenteECommerce);
      //  $deuxuneVenteECommerce = new VenteEcommerce("464",null, 350,"chez toi", "01");



        $unelisteFidele=array();

        $unelisteFidele[]=$this->conso1;
        $unelisteFidele[]=$this->conso2;
        $maStat= new Statistique();

    $this->assertEquals(1/4,$maStat->compareLieuVente($unelisteFidele),"probleme comparaison", 0.01);

    }

    public function testStatVente()
    {


    }

}
