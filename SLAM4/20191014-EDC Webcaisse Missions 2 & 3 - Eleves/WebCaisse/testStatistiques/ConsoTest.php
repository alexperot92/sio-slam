<?php
require "autoload.php" ;

use PHPUnit\Framework\TestCase;

class ConsoTest extends TestCase
{

protected $conso1, $conso1Vente1, $conso1Vente2 ;

protected function setUp() {
    $this->conso1 = new Conso("Robert","Timothee","tt","06");

}

    public function testGetNbVentes()
    {
        $this->assertEquals(0,$this->conso1->getNbVentes(),"erreur comptage");
        $uneVente = new VenteEcommerce(12114,$this->conso1,150);
        $this->conso1->addUneVente($uneVente);
        $this->assertEquals(0,$this->conso1->getNbVentes(),"erreur comptage");

        $uneVente = new VenteEcommerce(18114,$this->conso1,150);
        $this->conso1->addUneVente($uneVente);
        $this->assertEquals(0,$this->conso1->getNbVentes(),"erreur comptage");

    }


}
