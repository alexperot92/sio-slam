<?php

/**
 * Class Statistique
 */
class Statistique
{

// méthode statVente à commenter

    /**
     * @param $lesVentesDuJour
     * @return float|int
     */
    public function statVente($lesVentesDuJour)
    {
        $nbVenteFidele = 0;
        foreach ($lesVentesDuJour as $uneVente) {
            $conso = $uneVente->getLeConso();
            if ($conso->estFidele())
                $nbVenteFidele++;
        }
        return $nbVenteFidele * 100 / count($lesVentesDuJour);
    }


    /*
     *
     *
     *c'est une fonction qui renvoie une collection d'objet ayant pour paramètre lesVentesDuJour,
     *
     * on parcours la collection en affectant une vente sur le consommateur, si le conso est fidèle on ajoute une vente
     *
     */


// méthode compareLieuVente

    /**
     * @param $lesConsosFidele
     * @return float|int
     */
    public function compareLieuVente($lesConsosFidele)
    {
        $totalEcom = 0; //cumul des montants des ventes ecommerce
        $totalMag = 0;  // cumul des montants des ventes en magasin
//parcours de la liste des consommateurs fidèles
        foreach ($lesConsosFidele as $unConsoFidele) {
            // A COMPLETER

            foreach ($unConsoFidele->getVentes() as $uneVente) {
                if ($uneVente instanceof VenteEcommerce) {
                    $totalEcom=$totalEcom+$uneVente->getMontantVente();
                }
               else if ($uneVente instanceof VenteMagasin) {
                    $totalMag=$totalMag+$uneVente->getMontantVente();
                }
            }

        }
        return $totalMag / $totalEcom; //calcul de l’indice et retour du résultat
// la division par zero pose probleme
    }
}