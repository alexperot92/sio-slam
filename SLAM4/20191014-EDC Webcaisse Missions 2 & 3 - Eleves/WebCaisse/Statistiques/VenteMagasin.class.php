<?php

/**
 * Class VenteMagasin
 */
class VenteMagasin extends Vente {

    /**
     * @var
     */
    private $matriculeVendeur ;

    /**
     * VenteMagasin constructor.
     */
    public function __construct($uneDate,$unConso,$unMontant,$unmatricule)
    {
        parent::__construct($uneDate,$unConso,$unMontant);
        $this->matriculeVendeur=$unmatricule;
    }
}

