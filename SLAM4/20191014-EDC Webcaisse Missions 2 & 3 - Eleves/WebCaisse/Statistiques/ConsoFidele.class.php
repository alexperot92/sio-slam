<?php

/**
 * Class ConsoFidele
 */
class ConsoFidele extends Conso {

    /**
     * @var
     */
    private $dateNaissance ;
    /**
     * @var int
     */
    private $pointsFidelite=0 ;
    /**
     * @var
     */
    private $dateInscription ;

    public function __construct ($unNom, $unPrenom, $unmail, $untel, $dateNaiss, $dateInsc){
        parent::__construct($unNom, $unPrenom, $unmail, $untel);
        $this->dateNaissance = $dateNaiss ;
        $this->dateInscription = $dateInsc ;
    }

    /**
     * @return bool
     */
    public function estFidele() {
        return true;
    }

    /**
     * @param $typeFidelite
     * @param $montant
     */
    public function addFidelite($typeFidelite, $montant)  {
        // typeFidelite contient le type de programme de fidélisation choisi (1, 2 ou 3)
        // montant contient la valeur du montant d’achat réalisé
        switch ($typeFidelite) {
            case 1 :
                $this->pointsFidelite ++ ;
                break;
            case 2 :
                $this->pointsFidelite += $montant;
                break;
            case 3 :
                if ($montant >=100 && $montant <=200) {
                    $this->pointsFidelite += 10;
                }
                elseif ($montant >200 && $montant <=500){
                    $this->pointsFidelite += 20;
                }
                elseif ($montant > 500) {
                    $this->pointsFidelite += 50;
                }
                break;
        }
    }

    /**
     * @return int
     */
    public function getPointsFidelite() {
        return $this->pointsFidelite ;
    }


    public function __tostring () {
        return "Le nom est ".parent::getNom()."<BR>et le prénom est ".parent::getPrenom()."<BR>"."son mails est ".parent::getMail().
            " et son tel ".parent::getTel()." et le nombre de points de fidelite est: ".$this->getPointsFidelite();

    }

}