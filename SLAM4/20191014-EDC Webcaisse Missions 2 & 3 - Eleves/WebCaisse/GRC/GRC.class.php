<?php

class GRC extends PDORepository{
    private $lesConsos = array() ;
    public function listeConsoAFideliser(){
        //Création de la requête
        $requeteSql ="select distinct C.nom, C.prenom, C.tel, C.mail"
            . " from WEBCAISSE.Conso C "
            . "INNER join WEBCAISSE.Vente V on V.idConso = C.id"
            . " where C.id not in (select id from WEBCAISSE.ConsoFidele) "
            . " AND DATEDIFF(DAY, V.dateVente ,CONVERT(date, GETDATE())) < 30";

        //Exécution de la requête
        $statement = $this->queryList($requeteSql,PDO::FETCH_ASSOC,NULL) ;

        //Pour chaque enregistrement résultat de la requête
        while ($consoAFideliser = $statement->fetch()){
            //Création d’un objet de la classe Conso avec les données
            $leConso = new Conso($consoAFideliser['nom'],$consoAFideliser['prenom'],$consoAFideliser['tel'],$consoAFideliser['mail']) ;
            //ajout du Consommateur à la liste des Consommateurs
            $lesConsos[] = $leConso ;}
        // fermeture de la connexion à la BD
        $statement->closeCursor();
        return $lesConsos ;
    }
}

