public class Soldat{
    String nom;
    String grade;
    int sante;
    int force;
    int endurance;
    int competence;

    public Soldat(String nom, String grade, int sante, int force, int endurance, int competence) {
        this.nom = nom;
        this.grade = grade;
        this.sante = sante;
        this.force = force;
        this.endurance = endurance;
        this.competence = competence;
    }
}