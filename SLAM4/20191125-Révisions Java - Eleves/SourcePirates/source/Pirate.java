package source;

import java.util.Iterator;

public class Pirate extends Marin implements Piraterie {
    private int degredesauvagerie;
    private static final int seuilsauvagerie = 50;

    public Pirate(String nom, String fonction){
        super(nom, fonction);
    }
    public void pillage(Navire unNavire){
        Iterator <Marin> m = unNavire.getEquipage().iterator();
        if (degredesauvagerie > seuilsauvagerie){
            unNavire.getEquipage().clear();
        }
        else{
            while(m.hasNext()){
                Marin unMarin=m.next();
                if (unMarin instanceof Capitaine){
                    m.remove();
                }
            }
        }
    }

    public int getDegredesauvagerie() {
        return degredesauvagerie;
    }

    public void setDegredesauvagerie(int degredesauvagerie) {
        this.degredesauvagerie = degredesauvagerie;
    }

    @java.lang.Override
    public java.lang.String toString() {
        return "source.Pirate{" +
                "degredesauvagerie=" + degredesauvagerie +
                '}';
    }

    public boolean equals(Object object) {
        if (this == object) return true;
        if (!(object instanceof Pirate)) return false;
        if (!super.equals(object)) return false;
        Pirate pirate = (Pirate) object;
        return degredesauvagerie == pirate.degredesauvagerie && pirate.getFonction() == this.getFonction();
    }
}