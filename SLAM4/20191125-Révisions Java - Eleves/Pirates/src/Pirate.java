import java.util.Iterator;

public class Pirate extends Marin implements Piraterie {
    private int degredesauvagerie;
    private static final int seuilsauvagerie = 50;

    public Pirate(String nom, String fonction){
        super(nom, fonction);
    }

    public Pirate(String nom, String fonction, int degredesauvagerie) {
        super(nom, fonction);
        this.degredesauvagerie = degredesauvagerie;
    }

    public void pillage(Navire unNavire){
        Iterator <Marin> m = unNavire.getEquipage().iterator();
        if (degredesauvagerie > seuilsauvagerie){
            unNavire.getEquipage().clear();
        }
        else{
            while(m.hasNext()){
                Marin unMarin=m.next();
                if (unMarin instanceof Capitaine){
                    m.remove();
                }
            }
        }
    }

    public int getDegredesauvagerie() {
        return degredesauvagerie;
    }

    public void setDegredesauvagerie(int degredesauvagerie) {
        this.degredesauvagerie = degredesauvagerie;
    }

    @Override
    public String toString() {
        return "source.Pirate{" +
                "degredesauvagerie=" + degredesauvagerie +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pirate)) return false;

        Pirate pirate = (Pirate) o;

        return (degredesauvagerie == pirate.degredesauvagerie) && (pirate.getFonction() == this.getFonction());
    }

    @Override
    public int hashCode() {
        return degredesauvagerie;
    }
}