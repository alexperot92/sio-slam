
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;

public class testPirate {

    Pirate gargamel, rackam, barbeNoire ;
    Marin premierMarin,deuxiemeMarin;
    Navire nav;

    @BeforeEach
    public void setup(){

        gargamel = new Pirate("Gargamel","Dresseur de schtroumpf", 75) ;
        rackam = new Pirate("Rackam le rougae","Pirate sanguinaire", 75) ;
        barbeNoire = new Pirate("Barbe noire", "Pirate sanguinaire", 75) ;

        premierMarin= new Marin("Paul1","Marin violent");
        deuxiemeMarin= new Marin("Paul2","Marin violent");

        List<Marin> unEquipage = new ArrayList<>() ;
        unEquipage.add(premierMarin) ;
        unEquipage.add(deuxiemeMarin) ;

        nav = new Navire(15,"oui",unEquipage);
    }

    @Test
    public void testEgalitePirates(){

        Assertions.assertNotEquals(gargamel,rackam,"En fait si, ils sont égaux");
        Assertions.assertNotEquals(gargamel,barbeNoire,"En fait si, ils sont égaux");
        Assertions.assertEquals(barbeNoire,rackam,"En fait ils sont différents");

    }
    @Test
    public void ajoutMarin (){
        assertThat("Bonjour", nav.getEquipage(),empty());
        Marin unMarin = new Marin("ducro","mousse");
        Navire ajoutMarin(unMarin);
    }
    @Test
    public void retireMarin(){
        assertThat("Equipage non vide",unNavire.getEquipage(),empty());

    }
}
