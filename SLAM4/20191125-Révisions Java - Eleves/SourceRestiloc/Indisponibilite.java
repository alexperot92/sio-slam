public class Indisponibilite {
	private String motif;
	 private boolean clientResponsable;
	 //Constructeur qui permet d'instancier une indisponibilit� de RDV
	 public Indisponibilite(String unMotif, boolean clientResponsable) {
		 this.motif = unMotif;
		 this.clientResponsable = clientResponsable ;
		 
	 }
	 // indique si le client est responsable ou non de l�indisponibilit�
	 //<returns>True : client responsable / False : client non responsable</returns>
	 public boolean clientEstResponsable() { 
		 return this.clientResponsable ;
	 }
	 //Obtient le motif de l'indisponibilit�
	 public String getMotif()
	 {
		 return this.motif;
	 }
	@Override
	public String toString() {
		return "Indisponibilite [motif=" + motif + ", clientResponsable=" + clientResponsable + "]";
	}
	 
	 

}
