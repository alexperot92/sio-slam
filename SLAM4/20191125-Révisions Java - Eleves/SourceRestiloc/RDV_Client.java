import org.joda.time.DateTime ;


public class RDV_Client extends Expertise {
	
	private String nomContact;
	 private String telephone;
	 private String mail;

	
	public RDV_Client (String codeDossier, DateTime uneDateHeure, String unLieu,
			String uneAdresse, String uneImmat, String uneMarque, String unModele, int etatExpertise, String contact, String tel, String mail) 
	{
		
		super(codeDossier,uneDateHeure,unLieu,uneAdresse,uneImmat,uneMarque,unModele, etatExpertise) ;
		this.nomContact = contact ;
		this.telephone = tel ;
		this.mail = mail;
	}
	


}
