<?php
/**
 * Created by PhpStorm.
 * User: Timothee
 * Date: 02/11/2019
 * Time: 13:38
 */

class Club
{

    private $id;
    private $nom;
    private $adresse;
    private $email;
    private $laLigueRegionale; // ligue à laquelle le club appartient
    private $lesLicences;

    /**
     * Club constructor.
     * @param $id
     * @param $nom
     * @param $adresse
     * @param $email
     * @param $laLigueRegionale
     */
    public function __construct($id, $nom, $adresse, $email, $laLigueRegionale)
    {
        $this->id = $id;
        $this->nom = $nom;
        $this->adresse = $adresse;
        $this->email = $email;
        $this->laLigueRegionale = $laLigueRegionale;
        $this->lesLicences = array() ;
    } // array contenant les licences du club


    public function ajouterLicence($p_laLicence) {
        $this->lesLicences[]=$p_laLicence;
    }
    public function getNom() {
        return $this->nom;
    }


}