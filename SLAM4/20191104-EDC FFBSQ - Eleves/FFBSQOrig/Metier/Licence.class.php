<?php
/**
 * Created by PhpStorm.
 * User: Timothee
 * Date: 02/11/2019
 * Time: 13:42
 */

class Licence
{
    private $numero;
    private $annee;
    private $leClub;
    private $laCategorie; // catégorie du licencié
    private $lePratiquant;

    /**
     * Licence constructor.
     * @param $numero
     * @param $annee
     * @param $leClub
     * @param $laCategorie
     * @param $lePratiquant
     */
    public function __construct($numero, $annee, $leClub, $laCategorie, $lePratiquant)
    {
        $this->numero = $numero;
        $this->annee = $annee;
        $this->leClub = $leClub;
        $this->laCategorie = $laCategorie;
        $this->lePratiquant = $lePratiquant;
    } // pratiquant licencié


    public function getCategorie() {
        return $this->laCategorie;
    }
    public function getDescription() {
        return $this->numero." ".$this->lePratiquant->getNom()." ".$this->lePratiquant->getPrenom();
    }


}