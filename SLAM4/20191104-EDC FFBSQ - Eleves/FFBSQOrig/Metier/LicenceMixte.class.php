<?php
/**
 * Created by PhpStorm.
 * User: Timothee
 * Date: 02/11/2019
 * Time: 13:44
 */

class LicenceMixte extends Licence
{

    private $laEntreprise; // entreprise liée à la licence mixte
    public function __construct(	$p_numero,$p_annee,$p_leClub,$p_laCategorie,$p_lePratiquant,
                                    Entreprise $p_laEntreprise)
    {
        parent::__construct($p_numero,$p_annee,$p_leClub,$p_laCategorie,$p_lePratiquant);
        $this->laEntreprise=$p_laEntreprise;
        $this->laEntreprise->ajouterLicenceMixte($this);
    }



}