<?php
/**
 * Created by PhpStorm.
 * User: Timothee
 * Date: 02/11/2019
 * Time: 13:40
 */

class Pratiquant
{

    private $id;
    private $nom;
    private $prenom;
    private $adresse;
    private $email;
    private $lesLicences;

    /**
     * Pratiquant constructor.
     * @param $id
     * @param $nom
     * @param $prenom
     * @param $adresse
     * @param $email
     */
    public function __construct($id, $nom, $prenom, $adresse, $email)
    {
        $this->id = $id;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->adresse = $adresse;
        $this->email = $email;
        $this->lesLicences = array() ;
    } // array contenant les licences du pratiquant



    public function ajouterLicence($p_laLicence) {
        $this->lesLicences[]=$p_laLicence;
    }
    public function getNom() {
        return $this->nom;
    }
    public function getPrenom() {
        return $this->prenom;
    }


}