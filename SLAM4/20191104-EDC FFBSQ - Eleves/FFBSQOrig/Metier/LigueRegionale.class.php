<?php
/**
 * Created by PhpStorm.
 * User: Timothee
 * Date: 02/11/2019
 * Time: 13:35
 */

class LigueRegionale
{
    private $code;
    private $nom;
    private $adresse;
    private $email;
    private $lesClubs;

    /**
     * LigueRegionale constructor.
     * @param $code
     * @param $nom
     * @param $adresse
     * @param $email
     */
    public function __construct($code, $nom, $adresse, $email)
    {
        $this->code = $code;
        $this->nom = $nom;
        $this->adresse = $adresse;
        $this->email = $email;
        $this->lesClubs = array() ;
    } // array contenant les clubs de la ligue


    public function ajouterClub($p_leClub) {
        $this->lesClubs[]=$p_leClub;
    }
    public function getCode() {
        return $this->code;
    }
    public function getNom() {
        return $this->nom;
    }
    public function getLesClubs() {
        return $this->lesClubs;
    }


}


