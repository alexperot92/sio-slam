<?php
/**
 * Created by PhpStorm.
 * User: Timothee
 * Date: 02/11/2019
 * Time: 13:39
 */

class Categorie
{
    private $id;
    private $libelle;
    private $ageMin;
    private $ageMax;
    private $lesLicences;

    /**
     * Categorie constructor.
     * @param $id
     * @param $libelle
     * @param $ageMin
     * @param $ageMax
     */
    public function __construct($id, $libelle, $ageMin, $ageMax)
    {
        $this->id = $id;
        $this->libelle = $libelle;
        $this->ageMin = $ageMin;
        $this->ageMax = $ageMax;
        $this->lesLicences = array() ;
    } // array contenant les licences de la catégorie


    public function ajouterLicence($p_laLicence) {
        $this->lesLicences[]=$p_laLicence;
    }
    public function getLibelle() {
        return $this->libelle;
    }


}