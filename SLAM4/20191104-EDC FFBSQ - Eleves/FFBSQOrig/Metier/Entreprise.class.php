<?php
/**
 * Created by PhpStorm.
 * User: Timothee
 * Date: 02/11/2019
 * Time: 13:45
 */

class Entreprise
{
    private $id;
    private $nom;
    private $adresse;
    private $email;
    private $lesLicencesMixtes;

    /**
     * Entreprise constructor.
     * @param $id
     * @param $nom
     * @param $adresse
     * @param $email
     */
    public function __construct($id, $nom, $adresse, $email)
    {
        $this->id = $id;
        $this->nom = $nom;
        $this->adresse = $adresse;
        $this->email = $email;
        $this->lesLicencesMixtes = array() ;
    } // array contenant les licences mixtes de l’entreprise




    public function ajouterLicenceMixte($p_laLicence) {
        $this->lesLicencesMixtes[]=$p_laLicence;
    }
    public function getNom() {
        return $this->nom;
    }


}