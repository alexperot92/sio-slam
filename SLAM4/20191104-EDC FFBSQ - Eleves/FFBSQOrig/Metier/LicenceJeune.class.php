<?php
/**
 * Created by PhpStorm.
 * User: Timothee
 * Date: 02/11/2019
 * Time: 13:43
 */

class LicenceJeune extends Licence
{
    private $nomResp;
    private $prenomResp;
    private $telResp;
    public function __construct(	$p_numero,$p_annee,$p_leClub,$p_laCategorie,$p_lePratiquant,
                                    $p_nomResp,$p_prenomResp,$p_telResp)
    {
        parent::__construct($p_numero,$p_annee,$p_leClub,$p_laCategorie,$p_lePratiquant);
        $this->nomResp=$p_nomResp;
        $this->prenomResp=$p_prenomResp;
        $this->telResp=$p_telResp;
    }

}