<?php
require "autoload.php";

use PHPUnit\Framework\TestCase;

class LicenceTest extends TestCase
{
    private $licenceMixte ;
    private $licenceJeune ;
    private $uneEntreprise;
    private $unPratiquant ;
    private $leClub ;
    private $categorieJeune ;
    private $categorieVeteran ;
    private $uneLigueRegionale;

    public function setUp() {
        $this->uneEntreprise = new Entreprise("1","ENC","Bessieres","bessieres@enc.org") ;
        $this->leClub = new Club("017","ENC","BldBessieres","enc@bessieres.org","Paris") ;
        $this->unPratiquant = new Pratiquant("007","Robert","Timo", "MA", "robert.btssio@gmail.com") ;

    }

}
