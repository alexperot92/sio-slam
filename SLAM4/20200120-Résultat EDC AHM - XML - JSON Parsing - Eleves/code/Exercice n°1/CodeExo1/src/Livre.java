import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@XmlRootElement(name = "livre")
public class Livre {
    private String langue, titre, id, isbn, paysEdition, editeur;
    private Date EnregistrementLivre;
    private int prix;
    private List<String> lesAuteurs = new ArrayList<>();

    //Le constructeur
    public Livre(String langue, String titre, String id, String isbn, String paysEdition, String editeur, Date EnregistrementLivre, int prix, List<String> lesAuteurs) {
        this.langue = langue;
        this.titre = titre;
        this.id = id;
        this.isbn = isbn;
        this.paysEdition = paysEdition;
        this.editeur = editeur;
        this.EnregistrementLivre = EnregistrementLivre;
        this.prix = prix;
        this.lesAuteurs = lesAuteurs;
    }
    public Livre() {

    }

    //Les accesseurs
    @XmlAttribute(name="langage")
    public String getLangue() {
        return langue;
    }

    public void setLangue(String langue) {
        this.langue = langue;
    }
    @XmlElement
    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    @XmlAttribute
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    @XmlElement
    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }
    @XmlTransient
    public String getPaysEdition() {
        return paysEdition;
    }

    public void setPaysEdition(String paysEdition) {
        this.paysEdition = paysEdition;
    }
    @XmlElement
    public String getEditeur() {
        return editeur;
    }

    public void setEditeur(String editeur) {
        this.editeur = editeur;
    }
    @XmlElement(name="dateEnregistrement")
    public Date getEnregistrementLivre() {
        return EnregistrementLivre;
    }

    public void setEnregistrementLivre(Date enregistrementLivre) {
        EnregistrementLivre = enregistrementLivre;
    }
    @XmlElement
    public int getPrix() {
        return prix;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }
    @XmlElementWrapper(name="auteurs")
    @XmlElement(name="auteur")
    public List<String> getLesAuteurs() {
        return lesAuteurs;
    }

    public void setLesAuteurs(List<String> lesAuteurs) {
        this.lesAuteurs = lesAuteurs;
    }
    //Fin des accesseurs


    @Override
    public String toString() {
        return "Livre{" +
                "langue='" + langue + '\'' +
                ", titre='" + titre + '\'' +
                ", id='" + id + '\'' +
                ", isbn='" + isbn + '\'' +
                ", paysEdition='" + paysEdition + '\'' +
                ", editeur='" + editeur + '\'' +
                ", EnregistrementLivre=" + EnregistrementLivre +
                ", prix=" + prix +
                ", lesAuteurs=" + lesAuteurs +
                '}';
    }
}
