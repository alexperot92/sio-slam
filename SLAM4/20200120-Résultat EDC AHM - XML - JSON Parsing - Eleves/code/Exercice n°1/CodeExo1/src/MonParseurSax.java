import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MonParseurSax extends DefaultHandler {
    private List<Livre> livres;
    private String nomFichierLivreXML;
    private SimpleDateFormat sdf = new SimpleDateFormat("yy-MM-dd");
    private Livre unLivre;
    private String valeurLue;
    public static void main (String[] args){
        MonParseurSax unParser = new MonParseurSax("catalogue.xml");
    }
    private void parseDocument(){
        //parse
        SAXParserFactory factory = SAXParserFactory.newInstance();
        try{
            SAXParser parser = factory.newSAXParser();
            File leFichier = new File(nomFichierLivreXML);
            parser.parse(new InputSource(new FileInputStream(leFichier)),this);
        }catch(ParserConfigurationException e){
            System.out.println("ParserConfig error");
        }catch(SAXException e){
            System.out.println("SAXEXception : xml not wel formed");
        }catch(IOException e){
            System.out.println("IO error");
        }
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if(qName.equals("livre")){
            unLivre = new Livre();
            unLivre.setId(attributes.getValue("id"));
            unLivre.setLangue(attributes.getValue("langage"));

        }
        if(qName.equals("editeur")){
            unLivre.setPaysEdition(attributes.getValue("pays"));
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        valeurLue = new String (ch,start,length);
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if(qName.equals("livre")){
            livres.add(unLivre);
        }
        if(qName.equals("isbn")){
            unLivre.setIsbn(valeurLue);
        }
        if(qName.equals("titre")){
            unLivre.setTitre(valeurLue);
        }
        if(qName.equals("editeur")){
            unLivre.setEditeur(valeurLue);
        }
        if(qName.equals("prix")){
            unLivre.setPrix(Integer.parseInt(valeurLue));
        }
        if(qName.equals("auteur")){
            List<String> non = unLivre.getLesAuteurs();
            non.add(valeurLue);
            unLivre.setLesAuteurs(non);
        }
        if(qName.equals("dateEnregistrement")){
            Date date = null;
            try {
                date = sdf.parse(valeurLue);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            unLivre.setEnregistrementLivre(date);
        }

    }
    //Etape 9
    public void affichage(){
        for(Livre Lelivre : livres) {
            System.out.println(Lelivre.toString());
        }
    }
    //Etape 10

    public MonParseurSax(String oui) {
        nomFichierLivreXML = oui;
        livres = new ArrayList<>();
        parseDocument();
        affichage();
    }
}
