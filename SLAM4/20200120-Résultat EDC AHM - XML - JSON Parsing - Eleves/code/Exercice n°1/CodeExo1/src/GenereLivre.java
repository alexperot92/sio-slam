
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GenereLivre {
    public static void main (String[] args){
        SimpleDateFormat sdf = new SimpleDateFormat("yy-MM-dd");
        Date date = null;
        try {
            date = sdf.parse("1913-05-08");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        List<String> non = new ArrayList<>();
        non.add("Marcel Proust");
        Livre livre = new Livre("FR","A la recherche du temps perdu","004","978207072",null,"Gallimard",date,32,non);
        jaxbVersXml(livre);
    }



    private static void jaxbVersXml (Livre unLivre){
        try{
            JAXBContext jaxbContext = JAXBContext.newInstance(Livre.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            jaxbMarshaller.marshal(unLivre, new File("unLivre.xml"));
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
}
