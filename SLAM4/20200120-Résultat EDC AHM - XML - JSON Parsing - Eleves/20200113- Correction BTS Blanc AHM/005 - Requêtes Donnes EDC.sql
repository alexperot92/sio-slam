/*
IF EXISTS (SELECT name FROM sys.schemas WHERE name = N'AHM')
   BEGIN
      PRINT 'Destruction schema AHM'
      DROP SCHEMA AHM
END
GO
PRINT '    Creation Schema EDC AHM'
GO
CREATE SCHEMA AHM 
GO */

DROP SCHEMA IF EXISTS AHM 
GO
DROP TABLE IF EXISTS AHM.Stock;
GO
DROP TABLE IF EXISTS AHM.Decouper;
GO
DROP TABLE IF EXISTS AHM.Composer;
GO
DROP TABLE IF EXISTS AHM.Composant;
GO
DROP TABLE IF EXISTS AHM.MatierePremiere;
GO
DROP TABLE IF EXISTS AHM.LigneCommande;
GO
DROP TABLE IF EXISTS AHM.ModelePalette
GO
DROP TABLE IF EXISTS AHM.EssenceBois ;
GO
DROP TABLE IF EXISTS AHM.Commande ;
GO
DROP TABLE IF EXISTS AHM.Client ;
GO
DROP TABLE IF EXISTS AHM.Utilisateur ;
GO
DROP TABLE IF EXISTS AHM.Site ;

GO
CREATE SCHEMA AHM
GO

CREATE TABLE AHM.Site(
code INT PRIMARY KEY, 
nom VARCHAR(60));
GO
INSERT INTO AHM.SITE VALUES (1,'ESAT La Source') ;
INSERT INTO AHM.SITE VALUES (2,'ESAT Les M�sanges') ;
INSERT INTO AHM.SITE VALUES (3,'ESAT La For�t') ;



CREATE TABLE AHM.CLIENT
(
NUMERO INT PRIMARY KEY,
raisonSociale VARCHAR(60),
adresseRue VARCHAR(60),
codePostal CHAR(5),
ville VARCHAR(60),
telephone CHAR(10),
courriel VARCHAR(60),
imageLogo BINARY,
codeSite int FOREIGN KEY REFERENCES AHM.SITE(code)
);

GO
INSERT INTO AHM.CLIENT VALUES
(1,'ISOCOUSTIC','Province','27000','Quelque part','0229834527','contact@isocoustic.fr',NULL,2);


CREATE TABLE AHM.COMMANDE(
id INT PRIMARY KEY,
dateCommande DATE,
dateLivraisonPrevue DATE,
dateLivraisonReelle DATE DEFAULT NULL,
numeroClient int FOREIGN KEY REFERENCES AHM.CLIENT(numero)
);
GO
INSERT INTO AHM.COMMANDE VALUES(1,'2017-03-15','2017-04-08',NULL,1);
INSERT INTO AHM.COMMANDE VALUES(2,'2017-03-19','2017-04-12',NULL,1);



CREATE TABLE AHM.EssenceBois(
id INT NOT NULL PRIMARY KEY,
libelle VARCHAR(100)
);
GO
INSERT INTO AHM.EssenceBois VALUES (1,'H�tre');
INSERT INTO AHM.EssenceBois VALUES (2,'R�sineux');


CREATE TABLE AHM.ModelePalette (
id int NOT NULL PRIMARY KEY,
designation  VARCHAR(100),
longueur INT ,
largeur INT,
poidsCharge FLOAT,
coutRevient FLOAT,
idEssence INT FOREIGN KEY REFERENCES AHM.EssenceBois(id),
type CHAR(1), 
referenceCatalogue VARCHAR(60), 
nomFichierPdf VARCHAR(60), 
dateApplication DATE, 
dateCreation DATE, 
contact VARCHAR(60), 
numeroClient INT FOREIGN KEY REFERENCES AHM.CLIENT(numero)
);

INSERT INTO  AHM.ModelePalette VALUES
(1,'Palette bois personnalis�e en r�sineux 1200 x 1200 � plots',1200,1200,500,35,2,'P',NULL,
'Palette ISOCOUSTIC','2017-01-04','2016-10-02','Paul NIDGRON',1);


CREATE TABLE AHM.Composant(
id INT PRIMARY KEY NOT NULL, 
nomComposant VARCHAR(40),
longueur INT, 
largeur INT, 
epaisseur INT)
;

INSERT INTO AHM.Composant VALUES (1, 'Planche',1200,75,15 );
INSERT INTO AHM.Composant VALUES (2, 'Traverse',1200,80,15 );
INSERT INTO AHM.Composant VALUES (3, 'Ski',1200,75,15 );
INSERT INTO AHM.Composant VALUES (4, 'Plot',75,80,100 );

CREATE TABLE AHM.Composer(
idModelePalette INT NOT NULL FOREIGN KEY REFERENCES AHM.ModelePalette(id), 
idComposant INT NOT NULL FOREIGN KEY REFERENCES AHM.Composant(ID), 
quantite FLOAT ,
PRIMARY KEY(idModelePalette,idComposant)) ;
	
INSERT INTO AHM.Composer VALUES (1, 1,7 );
INSERT INTO AHM.Composer VALUES (1, 2,3 );
INSERT INTO AHM.Composer VALUES (1, 3,3 );
INSERT INTO AHM.Composer VALUES (1, 4,9 );




CREATE TABLE AHM.LigneCommande (
idCommande INT, 
idModele INT FOREIGN KEY REFERENCES AHM.ModelePalette(id), 
quantiteCommandee INT, 
prixUnitaireFacture FLOAT, 
estFabrique BIT DEFAULT 0,
CONSTRAINT Primaire PRIMARY KEY (idCommande,idModele)
);
INSERT INTO AHM.LigneCommande VALUES(1,1,4,25,1);
INSERT INTO AHM.LigneCommande VALUES(2,1,7,25.5,1);




CREATE TABLE AHM.MatierePremiere (
id INT PRIMARY KEY, 
libelle VARCHAR(100),
longueur INT, 
largeur INT, 
epaisseur INT, 
idEssence INT FOREIGN KEY REFERENCES AHM.EssenceBois(id)) ;

INSERT INTO AHM.MatierePremiere VALUES
(1, 'lamelle de bois de h�tre de longueur 800, largeur 70 et �paisseur 15',800,70,15,1) ;
INSERT INTO AHM.MatierePremiere VALUES
(2, 'lamelle de bois de h�tre de longueur 1200, largeur 70 et �paisseur 20',1200,70,20,1) ;
INSERT INTO AHM.MatierePremiere VALUES
(3, 'lamelle de bois de h�tre de longueur 1500, largeur 90 et �paisseur 15',1500,90,15,1) ;
INSERT INTO AHM.MatierePremiere VALUES
(4, 'lamelle de bois de r�sineux de longueur 800, largeur 70 et �paisseur 15',800,70,15,2) ;

CREATE TABLE AHM.Stock(
codeSite INT NOT NULL FOREIGN KEY REFERENCES AHM.Site(Code), 
idMatiere INT NOT NULL FOREIGN KEY REFERENCES AHM.MatierePremiere(id), 
stockActuel FLOAT, 
stockAlerte FLOAT,
CONSTRAINT PRIMARY_STOCK PRIMARY KEY (codeSite, idMatiere)
) ;

CREATE TABLE AHM.Decouper(
idMatiere INT NOT NULL FOREIGN KEY REFERENCES AHM.MatierePremiere(id), 
idComposant INT NOT NULL FOREIGN KEY REFERENCES AHM.Composant(id), 
quantiteDecoupee FLOAT, 
longueurPerte INT, 
longueurChute INT,
PRIMARY KEY (idMatiere,idComposant)
);

CREATE TABLE AHM.Utilisateur(
login VARCHAR(20) PRIMARY KEY,
motDePasse VARCHAR(20), 
nom VARCHAR(60), 
prenom VARCHAR(60), 
codeSite INT FOREIGN KEY REFERENCES AHM.SITE(code)) ;
	

INSERT INTO AHM.Utilisateur VALUES('laplace','laplace','LAPLACE', 'Jules',2) ;
INSERT INTO AHM.Utilisateur VALUES('comptable','expert','COMPTABLE', 'Mr',2) ;


