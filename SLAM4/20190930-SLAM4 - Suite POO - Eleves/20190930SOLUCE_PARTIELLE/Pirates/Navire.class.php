<?php
/**
 * Created by PhpStorm.
 * User: trobert
 * Date: 30/09/2019
 * Time: 11:59
 */

class Navire
{
    private $equipage = array() ;
    private $taille ;
    private $modele ;

    /**
     * Navire constructor.
     * @param array $equipage
     * @param $taille
     * @param $modele
     */
    public function __construct($equipage, $taille, $modele)
    {
        $this->equipage = $equipage;
        $this->taille = $taille;
        $this->modele = $modele;
    }

    /**
     * @return array
     */
    public function getEquipage()
    {
        return $this->equipage;
    }

    /**
     * @param array $equipage
     */
    public function setEquipage($equipage)
    {
        $this->equipage = $equipage;
    }

    /**
     * @return mixed
     */
    public function getTaille()
    {
        return $this->taille;
    }

    /**
     * @param mixed $taille
     */
    public function setTaille($taille)
    {
        $this->taille = $taille;
    }

    /**
     * @return mixed
     */
    public function getModele()
    {
        return $this->modele;
    }

    /**
     * @param mixed $modele
     */
    public function setModele($modele)
    {
        $this->modele = $modele;
    }

    public function ajoutMarin($unMarinAAjouter){
        array_push($this->equipage,$unMarinAAjouter) ;
    }


    public function __toString() {
        $aAfficher = "Le modèle est: ".$this->getModele()." et la taille est: ".$this->getTaille();

        foreach ($this->getEquipage() as $unMarin)
        {
            $aAfficher.=$unMarin."<BR>" ;
        }

    return $aAfficher ;

    }



}