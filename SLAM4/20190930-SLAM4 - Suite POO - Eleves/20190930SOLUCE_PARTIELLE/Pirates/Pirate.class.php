<?php
class Pirate extends Marin implements Piraterie{
    private $degressauvagerie;

    /**
     * Pirate constructor.
     * @param $degressauvagerie
     */
    public function __construct($nom,$uneFonction,$degressauvagerie)
    {
        parent(__construct($nom,$unFonction));
        $this->degressauvagerie = $degressauvagerie;
    }
    public function pillage(navire $unNavire){
        $nouvelequipage=array();
        if($this->getDegressauvagerie()>50){
            $uneNavire->setEquipage($nouvelequipage);
        }
        else{
            foreach($unNavire->getEquipage() as $unMarin){
                if($unMarin instanceof Capitaine){

                }
                else{
                    $newequipage[] = $unMarin;
                }
            }
            $unNavire->setEquipage($newequipage);
        }
    }

    /**
     * @return mixed
     */
    public function getDegressauvagerie()
    {
        return $this->degressauvagerie;
    }

    /**
     * @param mixed $degressauvagerie
     */
    public function setDegressauvagerie($degressauvagerie)
    {
        $this->degressauvagerie = $degressauvagerie;
    }

    public function __toString()
    {
        // TODO: Implement __toString() method.
        parent::__toString()." est devenue un pirate sauvage de degrès ".$this->setDegressauvagerie();
    }

}
?>