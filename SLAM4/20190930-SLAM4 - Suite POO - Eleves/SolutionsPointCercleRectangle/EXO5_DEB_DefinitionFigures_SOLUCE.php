<?php

class Point {
private $abscisse;
private $ordonnee;

function __construct($uneAbscisse, $uneOrdonnee)
{
  $this->abscisse = $uneAbscisse ;
  $this->ordonnee = $uneOrdonnee;
}

function getAbscisse(){
  return $this->abscisse ;
}

function getOrdonnee(){
  return $this->ordonnee ;
}

function __toString() {
  return "l' abscisse du point est ".$this->abscisse." et son ordonnée ".$this->ordonnee." <BR>";
}

}

class Rectangle {
private $pointHautGauche;
private $pointBasDroite;

function __construct($premierPoint, $deuxiemePoint)
{
  $this->pointHautGauche = $premierPoint ;
  $this->pointBasDroite = $deuxiemePoint;
}

function getPointHautGauche() {
  return $this->pointHautGauche ;
}

function getPointBasDroite() {
  return $this->pointBasDroite ;
}

function __toString() {
  return "Rectangle avec point haut gauche est ".$this->pointHautGauche." et pointHautDroite ".$this->pointBasDroite." <BR>";
}

}

class Cercle {
  private $pointCentre;
  private $rayon ;

  function __construct ($point, $rayon){
    $this->pointCentre = $point ;
    $this->rayon = $rayon;
  }


  function getPointCentre(){
    return $this->pointCentre;
  }

  function getRayon(){
    return $this->rayon ;
  }

  function surfaceCercle(){
    // Calcul de la surface
      return (M_PI * $this->rayon * $this->rayon);

  }

  function perimetreCercle(){
    // Calcul du périmètre
      return (2* M_PI * $this->rayon) ;

  }


}


?>
