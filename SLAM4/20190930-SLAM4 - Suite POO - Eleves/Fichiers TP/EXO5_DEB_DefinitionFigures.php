<?php

class Point {
private $abscisse;
private $ordonnee;




function __construct($uneAbscisse, $uneOrdonnee)
{
  $this->abscisse = $uneAbscisse ;
  $this->ordonnee = $uneOrdonnee;
}

function getAbscisse(){
  return $this->abscisse ;
}

function getOrdonnee(){
  return $this->ordonnee ;
}
function __toString(){
  return "abscisse du point est ".$this->abscisse." et son ordonnée ".$this->ordonnee." <br>";
}

}

class Rectangle {
private $pointHautGauche;
private $pointBasDroite;



function __construct($premierPoint, $deuxiemePoint)
{
  $this->pointHautGauche = $premierPoint ;
  $this->pointBasDroite = $deuxiemePoint;
}

function getPointHautGauche() {
  return $this->pointHautGauche ;
}

function getPointBasDroite() {
  return $this->pointBasDroite ;
}
public function __toString(){
    return "Le premier point est".$this->pointHautGauche." est le second point est ".$this->pointBasDroite;
}

}

class Cercle {
    private $pointCentre;
    private $rayon ;

    function __construct ($point, $rayon){
        $this->pointCentre = $point ;
        $this->rayon = $rayon;
    }


    function getPointCentre(){
        return $this->pointCentre;
    }

    function getRayon(){
        return $this->rayon ;
    }

   // Calcul du périmètre
    function perimetre(){
      $perimetre=2*M_PI*$this->getRayon();
    }

    // Calcul de la surface
    function surface(){
      $surface=M_PI*pow($this->getRayon(),2);
    }
    
}


?>
