<?php 
    interface Nominterface {
        public function getNom();
        public function setNom($nimporteNom);
    }
    class livre implements NomInterface{
        private $nomLivre;
        public function getNom(){
            return $this->nomLivre;
        }
        public function setNom($nimporteNom){
            $this->nomLivre = $nimporteNom;
        }
        public function afficheNom(){
            echo "Mon livre préféré est:".$this->getNom()."<br>";
        }
    }
?>