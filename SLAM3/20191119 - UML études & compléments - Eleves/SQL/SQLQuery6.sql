CREATE OR ALTER PROCEDURE ELEVE_FORME (@FILIERE VARCHAR(10) , @RESULTAT VARCHAR(50) OUTPUT) AS
BEGIN
Declare @COMPTE int
	IF (@FILIERE is null)
	select @RESULTAT=CONCAT(FMT.NOM_ELEVE, '-',FMT.CODE_FILIERE) FROM PROCS.FORMATIONELEVES FMT
	where FMT.NB_HEURES_COURS_INFO = (select MAX(NB_HEURES_COURS_INFO) FROM PROCS.FORMATIONELEVES)
	Else
	SELECT @COMPTE = COUNT(*) FROM(
	SELECT FMT.NOM_ELEVE FROM PROCS.FORMATIONELEVES FMT
	WHERE FMT.NB_HEURES_COURS_INFO = (select MAX(NB_HEURES_COURS_INFO) FROM PROCS.FORMATIONELEVES where FMT.CODE_FILIERE = @filiere )) AS SOUS
	IF (@COMPTE > 1)
	SET @RESULTAT = 'Plusieurs �l�ves ex aequo'
	ELSE SET @RESULTAT = 'A faire'
END

GO
DECLARE @sortie VARCHAR(50)
EXEC ELEVE_FORME NULL, @sortie OUTPUT
PRINT @sortie

GO
DECLARE @sortie VARCHAR(50)
EXEC ELEVE_FORME 'SLAM ENC', @sortie OUTPUT
PRINT @sortie

GO
DECLARE @sortie VARCHAR(50)
EXEC ELEVE_FORME 'Ecole 42', @sortie OUTPUT
PRINT @sortie