CREATE TABLE RESTILOC.PREE (
	PK_numeroOrdre int primary KEY Identity(1,1),
	libelle int,
	description_PREE VARCHAR(70)
);

CREATE TABLE RESTILOC.PREE_PEINTURE (
	PK_PREE_PEINTURE int primary KEY Identity(1,1),
	fk_libelle int,
	fk_description_PREE VARCHAR(70)
);

CREATE TABLE RESTILOC.PREE_PIECE (
	PK_PREE_PIECE int primary key identity(1,1),
	fk_libelle int,
	fk_description_PREE VARCHAR(70)
);

