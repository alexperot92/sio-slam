<?php
abstract class PDORepository{
    const USERNAME="aperot92i";
    const PASSWORD="V4t3f41r3v01r3";
    const SERVER="SQLSRV2";
    const DB="APEROT";

    private function getConnection(){
        $username = self::USERNAME;
        $password = self::PASSWORD;
        $server = self::SERVER;
        $db = self::DB;
        try {
            $connection = new PDO("sqlsrv:server=$server,1433;Database=$db", $username, $password);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo 'Échec lors de la connexion : ' . $e->getMessage();
        }
        return $connection;
    }
    protected function queryList($sql, $fetch_mode,$nom_classe){
        $connection = $this->getConnection();
        $stmt = $connection->prepare($sql);
        if ($nom_classe === NULL && $fetch_mode != PDO::FETCH_COLUMN)
        {
            $stmt->setFetchMode($fetch_mode);
        }
        else if ($fetch_mode == PDO::FETCH_COLUMN) {
            $stmt->setFetchMode($fetch_mode,0);

        }
        else
        {
            $stmt->setFetchMode($fetch_mode|PDO::FETCH_PROPS_LATE,$nom_classe);
        }
        $stmt->execute();
        return $stmt;
    }
    function insertion(FormationEleve $eleve1){
        $connection = $this->getConnection();
        $stmt = $connection->prepare("INSERT INTO PROCS.FORMATIONELEVES(NOM_ELEVE,NB_HEURES_COURS_INFO,CODE_FILIERE) VALUES (:nom,:nbh,:code)");
        $nom = $eleve1->getNom_eleve();
        $heures = $eleve1->getNb_heures_cours_info();
        $code = $eleve1->getCode_filiere();
        


        $stmt->bindParam(":nom",$nom,PDO::PARAM_STR);
        $stmt->bindParam(":nbh",$heures,PDO::PARAM_STR);
        $stmt->bindParam(":code",$code,PDO::PARAM_STR);

        $retour = $stmt->execute();

        if($retour){
            echo "ça semble ok ";
        }
        else{
            echo "pas ok";
        }
    }
}

?>
