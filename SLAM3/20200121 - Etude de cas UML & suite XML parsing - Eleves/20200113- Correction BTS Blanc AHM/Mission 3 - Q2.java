// Parcours de la collection "lesLignes"
// LigneCommande: type de la collection
// uneLigne : nom de la variable temporaire de type LigneCommande utilisé lors de l'itération
// lesLignes: nom de la collection d'objets LigneCommande

for (LigneCommande uneLigne : lesLignes)
{

// on récupère l'attribut MatierePremiere de la classe LigneCommmande
// on le convertit en String car lesDescriptions est une collectin de String
// (cf Annexe 5: ArrayList<String> lesDescriptions = new ArrayList<String>(); )
// on l'ajoute à la collection lesDescriptions via la méthode add: un exemple
// était donné dans l'annexe 5

lesDescriptions.add(uneLigne.getLaMatierePremiere().toString());		
}


// Création d'un objet "Adaptateur de données" qui va adapter les données de la 
// collection des descriptions de commandes pour une ListView 
// (paramètre "android.r.layout.simple_list_item_1")
ArrayAdapter<String> monAdaptateur = new ArrayAdapter<String> (this, 
android.R.layout.simple_list_item_1, lesDescriptions);	


// Affectation, à la ListView (de nom "listeLignes"), d'une entête
// contenant le texte "Contenu commande" suivi du numéro de la commande

// Le R.id.listeLignes est à trouver sur la maquette de l'écran 2 dans l'annexe 4, sur la droite
ListView listeViewLignes = (ListView) findViewById(R.id.listeLignes);

// [remarque: pas de cast nécessaire sur les dernières versins d'Android: cf TP]

TextView textView = new TextView(this); 	
textView.setText("Contenu commande n°" + numeroCommande);
listeViewLignes.addHeaderView(textView);

// Affectation de l'objet Adaptateur à la ListView (de nom "listeLignes")
listeViewLignes.setAdapter(monAdaptateur); 
