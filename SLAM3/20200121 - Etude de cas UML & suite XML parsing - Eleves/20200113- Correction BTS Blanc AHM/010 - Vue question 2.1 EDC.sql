CREATE VIEW AHM.stockPalettes
AS
	SELECT SUM(quantiteCommandee) nbPalettes, SUM(quantiteCommandee*coutRevient) coutRevientTotal
	FROM AHM.Commande
	INNER JOIN AHM.ligneCommande ON Commande.id = LigneCommande.idCommande
 	INNER JOIN AHM.ModelePalette ON ligneCommande.idModele = ModelePalette.id
	WHERE estFabrique = 1
	AND dateLivraisonReelle IS NULL;
	GO
	select * from AHM.stockPalettes
