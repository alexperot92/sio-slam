// Ajout de la quantité saisie à l’objet uneLigneCommande
// cf dernière méthode,ajoutQuantiteMesuree(),  de la classe LigneCommmande en annexe 3
// description des classes métier
uneLigneCommande.ajoutQuantiteMesuree(quantiteMesureeSaisie);

// Affectation à l’objet TextView tvTotalLivre du texte contenant la quantité totale livrée
TextView tvTotalLivre;
tvTotalLivre = (TextView) findViewById(R.id.totalLivre);
tvTotalLivre.setText("Quantite totale livrée : " + 
		String.valueOf(uneLigneCommande.getQuantiteReelleLivree( ) + " m3"); 	

// Mise à jour de la base de données par l’utilisation de la méthode statique miseAJour 
// de la classe LigneCommandeDAO

// ici il faut remarquer que miseAJour est une méthode statique
// cf signature: public static void miseAJour(LigneCommande uneLigne) 
// donc elle s'applique à la classe elle même et non pas à une instance de classe (objet)

LigneCommandeDAO.miseAJour(uneLigneCommande);

