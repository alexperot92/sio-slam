CREATE OR ALTER TRIGGER MERSOLEIL.HistoLocation
ON MERSOLEIL.Location
FOR DELETE
AS
INSERT INTO MERSOLEIL.HistoLoc (DateHisto,Numloc, NumSem, NumStu, NumCli, Regle, PrixLoc)
SELECT GetDate(),Numloc, NumSem, NumStu, NumCli, Regle, PrixLoc
FROM deleted

