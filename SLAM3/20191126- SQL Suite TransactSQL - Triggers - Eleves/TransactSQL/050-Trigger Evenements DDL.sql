CREATE TABLE evenements
(
    moment     DATETIME2 NOT NULL DEFAULT CURRENT_TIMESTAMP,
    type       VARCHAR(64),
    evenement     VARCHAR(MAX),
    evenementXML  XML,
    base         VARCHAR(255),
    objet       VARCHAR(255),
    programme  VARCHAR(255),
    login    VARCHAR(255));
GO

CREATE TRIGGER E_DDL_surveille
   ON DATABASE
   AFTER DROP_TABLE, DROP_INDEX, ALTER_TABLE
AS
BEGIN
 SET NOCOUNT ON;
 DECLARE @eventdata XML = EVENTDATA();
  INSERT INTO evenements 
    (type,evenement,evenementXML,base,objet,programme,login)  
  SELECT
     @EventData.value('(/EVENT_INSTANCE/EventType)[1]','VARCHAR(100)'),
     @EventData.value('(/EVENT_INSTANCE/TSQLCommand)[1]','VARCHAR(MAX)'),
     @EventData,
     DB_NAME(),
      @EventData.value('(/EVENT_INSTANCE/ObjectName)[1]','VARCHAR(255)'),
     PROGRAM_NAME(),
     SUSER_SNAME();
 IF (DATEPART(weekday,GETDATE())) IN (1,2)
  BEGIN
   COMMIT TRANSACTION;
   THROW 50004,'Vous ne devriez pas d�truire des tables, petits padawans...',1;
 END;
END;
GO

/* DROP TRIGGER E_DDL_surveille ON DATABASE; */
GO