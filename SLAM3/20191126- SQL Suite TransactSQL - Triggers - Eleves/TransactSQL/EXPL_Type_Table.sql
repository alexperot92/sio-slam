CREATE TYPE SECTION_BTS AS TABLE   
    ( 
	PK_ID_SECTION INT NOT NULL IDENTITY(1,1),
	NOM_SECTION VARCHAR(50) , 
    EFFECTIF_MAXIMUM INT,
	NB_HEURES_COURS INT );  
GO