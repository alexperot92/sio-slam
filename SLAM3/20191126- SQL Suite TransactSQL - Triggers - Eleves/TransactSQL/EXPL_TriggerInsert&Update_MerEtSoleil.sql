CREATE TRIGGER NomEnMaj
ON MERSOLEIL.Client
FOR INSERT, UPDATE
AS
UPDATE MERSOLEIL.Client
SET NomCli = UPPER(inserted.NomCli)
FROM Client, inserted
WHERE Client.NumCli = inserted.NumCli

