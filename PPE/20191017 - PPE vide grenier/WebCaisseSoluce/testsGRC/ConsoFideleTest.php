<?php
use PHPUnit\Framework\TestCase;
require "autoload.php" ;

class ConsoFideleTest extends TestCase
{
    public function testAddFideliteTampon() {
        $consoTest = new ConsoFidele("Lifo", "Paul","lifo.paul@gmail.com", "0600000000","1961-01-03","2017-01-05");
        $consoTest->addFidelite(1, 50);
        $this->assertEquals(1,$consoTest->getPointsFidelite(),"erreur calcul 1er tampon");
        $consoTest->addFidelite(1, 20);
        $this->assertEquals(2,$consoTest->getPointsFidelite(),"erreur calcul 2ème tampon");
    }


    public function testAddFideliteMontant() {
        $consoTest = new ConsoFidele("Lifo", "Paul","lifo.paul@gmail.com", "0600000000","1961-01-03","2017-01-05");
        $consoTest->addFidelite(2, 150);
        $this->assertEquals(150,$consoTest->getPointsFidelite(),"erreur calcul 1er achat");
        $consoTest->addFidelite(2, 250);
        $this->assertEquals(400,$consoTest->getPointsFidelite(),"erreur calcul 2ème  achat");
    }

    public function testInitConso() {
        $consoTest = new ConsoFidele("Lifo", "Paul","lifo.paul@gmail.com", "0600000000","1961-01-03","2017-01-05");
        // A COMPLETER
        $this->assertEquals(0, $consoTest->getPointsFidelite(),"Erreur à l'initialisation");


    }

    public function testAddMontant() {
        $consoTest = new ConsoFidele("Lifo", "Paul","lifo.paul@gmail.com", "0600000000","1961-01-03","2017-01-05");
        // A COMPLETER CI DESSOUS

        $consoTest->addFidelite(3, 50);
        $this->assertEquals(0, $consoTest->getPointsFidelite(),"Erreur 1er achat (inf 100 euros)");


        $consoTest->addFidelite(3, 150);
        $this->assertEquals(10, $consoTest->getPointsFidelite(),"Erreur 2ème achat (Entre 100 et 200 euros)");


        $consoTest->addFidelite(3, 250);
        $this->assertEquals(30, $consoTest->getPointsFidelite(),"Erreur 3ème achat (Entre 200 et 500 euros)");


        $consoTest->addFidelite(3, 550);
        $this->assertEquals(80, $consoTest->getPointsFidelite(),"Erreur 4ème achat (Supérieur à 500 euros)");

    }

}
