<?php

/**
 * Class Statistique
 */
class Statistique {

// méthode statVente à commenter

    /**
     * @param $lesVentesDuJour
     * @return float|int
     */
    public function statVente($lesVentesDuJour) {
        $nbVenteFidele=0;
        foreach($lesVentesDuJour as $uneVente) {
            $conso = $uneVente->getLeConso();
            if ($conso->estFidele())
                $nbVenteFidele ++;
        }
        return $nbVenteFidele*100/count($lesVentesDuJour);
    }

// méthode compareLieuVente

    /**
     * @param $lesConsosFidele
     * @return float|int
     */
    public function compareLieuVente($lesConsosFidele) {
        $totalEcom=0; //cumul des montants des ventes ecommerce
        $totalMag=0;  // cumul des montants des ventes en magasin
        //parcours de la liste des consommateurs fidèles
        foreach($lesConsosFidele as $unConsoFidele) {
            foreach($unConsoFidele->getVentes() as $uneVente )
            {
                if ($uneVente instanceof VenteEcommerce ) // vente ecommerce
                    //on ajoute le total de la vente au cumul ecommerce
                    $totalEcom += $uneVente->getMontantVente();
                else
                    //on ajoute le total de la vente au cumul magasin
                    $totalMag += $uneVente->getMontantVente();
            }
        }
        return $totalMag/$totalEcom ; //calcul de l’indice et retour du résultat
    }

}