<?php

/**
 * Class Vente
 */
abstract class Vente {

    /**
     * @var
     */
    private $dateVente ;
    /**
     * @var
     */
    private $leConso ;
    /**
     * @var
     */
    private $montant ;

    public function __construct ($uneDateVente,$unConso, $unMontant){
        $this->dateVente = $uneDateVente ;
        $this->leConso = $unConso ;
        $this->montant = $unMontant ;

    }

    public function getMontantVente() {
        return $this->montant ;
    }

    public function getLeConso() {
        return $this->leConso ;
    }

}