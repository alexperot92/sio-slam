<?php

/**
 * Class VenteMagasin
 */
class VenteMagasin extends Vente
{

    /**
     * @var
     */
    private $matriculeVendeur;

    /**
     * VenteMagasin constructor.
     */
    public function __construct($uneDateVente, $unConso, $unMontant, $unMatricule)
    {
// A COMPLETER SUR VOTRE COPIE
        parent::__construct($uneDateVente, $unConso, $unMontant);
        $this->matriculeVendeur = $unMatricule;
    }
}

