<?php
/**
 * Class Conso
 */
class Conso {
    private $nom ;
    /**
     * @var
     */
    private $prenom ;
    /**
     * @var
     */
    private $mail ;
    /**
     * @var
     */
    private $tel ;
    /**
     * @var array
     */
    private $lesVentes = array() ;

    public function __construct($unNom, $unPrenom, $unmail, $untel) {
        $this->nom = $unNom ;
        $this->prenom = $unPrenom ;
        $this->mail = $unmail ;
        $this->tel = $untel ;
    }

    protected function getNom(){
        return $this->nom ;
    }

    protected function setNom($unNom){
        $this->nom = $unNom ;
    }

    protected function getPrenom(){
        return $this->prenom ;
    }

    protected function setPrenom($unPrenom){
        $this->prenom = $unPrenom ;
    }


    protected function getMail(){
        return $this->mail ;
    }

    protected function setMail($unMail){
        $this->mail = $unmail ;
    }

    protected function getTel(){
        return $this->tel ;
    }

    protected function setTel($unTel){
        $this->tel = $unTel ;
    }

    /**
     * @return bool
     */
    public function estFidele() {
        return false;
    }

    /**
     * @param $uneVente
     */
    public function addUneVente($uneVente) {
        $this->lesVentes[] = $uneVente;

    }

    /**
     * @return array
     */
    public function getVentes() {
        return $this->lesVentes ;
    }

    /**
     *
     */
    public function getNbVentes() {
        // A COMPLETER
        return count($this->lesVentes) ;
    }

    public function getVentesSupMt($montant)
    {
        $lesVentesSel = array();
        foreach($this->lesVentes as $uneVente) //parcours de la liste
        {
            if ($uneVente->getMontantVente() > $montant)
//on ajoute la vente à la collection
            { $lesVentesSel[]=$uneVente->getMontantVente() ; }
        }
        return $lesVentesSel ;

    }


    public function __tostring () {
        return "Le nom est ".$this->getNom()."<BR>et le prénom est ".$this->getPrenom()."<BR>"."son mails est ".$this->getMail().
            " et son tel ".$this->getTel()." et le nombre de points de fidelite est: 0";

    }


}