<?php
require "autoload.php";

use PHPUnit\Framework\TestCase;

class ConsoTest extends TestCase
{
    protected $timoConso,$geradlineConso,$mrtoutlemondeConso,$venteTim1 ;

    protected function setUp() {
        $this->timoConso = new ConsoFidele("Robert","Timothee","robert.btssio@gmail.com", "0615151515","1971-06-15","2018-01-05") ;
        $this->geradlineConso = new ConsoFidele("Durand","Geraldine","geraldine.durand@gmail.com", "0620253035","1977-05-04","2018-05-05") ;
        $this->mrtoutlemondeConso = new Conso("Dupond","Gerard","gerard.dupond@gmail.com", "0635353535") ;



    }

    public function testGetNbVentes()
    {

        /*
        $this->venteTim1 = new VenteEcommerce("2018-10-31", $this->timoConso, 150 ,"MaisonsAlfort", "Colissimo") ;
        $this->venteTim2 = new VenteEcommerce("2018-11-02", $this->timoConso, 80 ,"MaisonsAlfort", "Relais colis") ;
        $this->venteTim3 = new VenteEcommerce("2018-11-03", $this->timoConso, 45 ,"MaisonsAlfort", "Poste") ;
        $this->venteGer1 = new VenteMagasin("2018-11-02", $this->geradlineConso, 125 ,"SuperVendeur74") ;
        $this->ventelambda1 = new VenteMagasin("2018-11-02", $this->mrtoutlemondeConso, 65 ,"SuperVendeur94") ;
        */

        $this->venteTim1 = new VenteEcommerce("2018-10-31", null, 150 ,"MaisonsAlfort", "Colissimo") ;
        $this->venteTim2 = new VenteEcommerce("2018-11-02", null, 80 ,"MaisonsAlfort", "Relais colis") ;
        $this->venteTim3 = new VenteEcommerce("2018-11-03", null, 45 ,"MaisonsAlfort", "Poste") ;
        $this->venteGer1 = new VenteMagasin("2018-11-02", null, 125 ,"SuperVendeur74") ;
        $this->ventelambda1 = new VenteMagasin("2018-11-02", null, 65 ,"SuperVendeur94") ;


        $this->assertEquals(0,$this->timoConso->getNbVentes(),"Problème comptage ou initialisation des ventes") ;
        $this->timoConso->addUneVente($this->venteTim1) ;
        $this->assertEquals(1,$this->timoConso->getNbVentes(),"Problème comptage ou initialisation des ventes") ;
        $this->timoConso->addUneVente($this->venteTim2) ;
        $this->timoConso->addUneVente($this->venteTim3) ;

        $this->geradlineConso->addUneVente($this->venteGer1) ;
        $this->mrtoutlemondeConso->addUneVente($this->ventelambda1) ;

        $this->assertEquals(3,$this->timoConso->getNbVentes(),"Problème comptage ou initialisation des ventes") ;
    }



}
