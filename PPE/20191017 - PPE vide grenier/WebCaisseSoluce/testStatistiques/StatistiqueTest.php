<?php


use PHPUnit\Framework\TestCase;
require "autoload.php" ;

class StatistiqueTest extends TestCase
{
    public $conso1, $conso2, $mrtoutlemondeConso ;

    public function setUp() {
        $this->conso1 = new ConsoFidele("Dupond","gérard","robert.btssio@gmail.com", "0615151515","1971-06-15","2018-01-05") ;
        $this->conso2 = new ConsoFidele("Durand","Geraldine","geraldine.durand@gmail.com", "0620253035","1977-05-04","2018-05-05");
        $this->mrtoutlemondeConso = new Conso("Dupond","Gerard","gerard.dupond@gmail.com", "0635353535") ;

    }

    public function testCompareLieuVente()
    {

        $vente1Conso1 = new VenteEcommerce("2018-10-31", null, 100 ,"MaisonsAlfort", "Colissimo") ;
        $vente2Conso1 = new VenteMagasin("2019-05-12", null, 80 ,"Gépéto") ;
        $vente1Conso2 = new VenteMagasin("2018-11-02", null, 120 ,"SuperVendeur74") ;

        $this->conso1->addUneVente($vente1Conso1);
        $this->conso2->addUneVente($vente1Conso2);

        $lesConsosFidele = array() ;

        $lesConsosFidele[] = $this->conso1 ;
        $lesConsosFidele[] = $this->conso2 ;

        $mesStats = new Statistique() ;

        $this->assertEquals(1.2,$mesStats->compareLieuVente($lesConsosFidele),"Probleme de calcul comparaison lieu vente") ;
        $this->conso1->addUneVente($vente2Conso1);
        $this->assertEquals(2,$mesStats->compareLieuVente($lesConsosFidele),"Probleme de calcul comparaison lieu vente") ;


    }

    public function testStatVente()
    {
        $vente1Conso1 = new VenteEcommerce("2018-10-31", $this->conso1, 150 ,"MaisonsAlfort", "Colissimo") ;
        $vente1Conso2 = new VenteMagasin("2018-11-02", $this->conso2, 125 ,"SuperVendeur74") ;
        $ventelambda1 = new VenteMagasin("2018-11-02", $this->mrtoutlemondeConso, 65 ,"SuperVendeur94") ;

        $lesVentesDuJour = array() ;
        $lesVentesDuJour[] =  $vente1Conso1 ;

        $mesStats = new Statistique() ;

        $this->assertEquals(100,$mesStats->statVente($lesVentesDuJour),"Probleme de calcul coco") ;
        $lesVentesDuJour[] =  $ventelambda1 ;
        $this->assertEquals(50,$mesStats->statVente($lesVentesDuJour),"Probleme de calcul coco") ;
        $lesVentesDuJour[] =  $vente1Conso2 ;
        $this->assertEquals(66.66,$mesStats->statVente($lesVentesDuJour),"Probleme de calcul coco",0.01) ;

    }
}
