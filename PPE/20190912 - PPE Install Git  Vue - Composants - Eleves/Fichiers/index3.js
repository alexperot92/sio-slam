Vue.component('competence', {
	props:['titre','contenu'],
	data() {
		return {
			voirCompetence: true
		}

},
template: `
<article class="message">
	<div class="message-header">
	{{ titre }}
	<span class="close">x</span>
	</div>
	<div class="message-body">
	{{ contenu }}
	</div>
	</article>
`
})
var app = new Vue({
	el: '#root'
})