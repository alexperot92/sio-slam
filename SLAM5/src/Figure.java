//Déclaraion de la classe abstraite Figure
abstract class Figure {
    //Déclaration des méthodes abstraites
    abstract void Calcul_Perimetre();
    abstract void Calcul_Surface();
    abstract void Doubler_Perimetre();
    abstract void Affiche();
}
//Déclaration de la classe abstraite FigureBis
abstract class FigureBis extends Figure{
    //Déclaration de ses attributs
    protected int xA;
    protected int xB;
    protected int yA;
    protected int yB;
    //Déclaration des constructeurs
    public FigureBis (int xASaisi, int yASaisi, int xBSaisi, int yBSaisi){
        xA = xASaisi;
        yA = yASaisi;
        xB = xBSaisi;
        yB = yBSaisi;
    }
    //Déclaration des méthodes
    public void Calcul_Perimetre(){
        int peri;
        peri= (xB-xA)+(yB-yA)*2;
        System.out.println("Le périmètre de votre figure vaut"+peri);
    }
    public void Calcul_Surface(){
        int surf;
        surf = (xB-xA)*(yB-yA);
        System.out.println("La surface de votre figure vaut"+surf);
    }
    public void Doubler_Perimetre(){
        int NouvPeri;
        if(xB>xA) xB = (xB-xA)*2 + xA ; else xA = (xA-xB)*2 + xB;
        Calcul_Perimetre();
    }
    abstract void Affiche(); //Cette méthode n'est pas obligatoire
}
//Déclaration de la classe Rectangle
class Rectangle extends FigureBis{
    //Déclaration des attributs
    int l,L;
    //Déclarationdu constructeur


    public Rectangle(int xASaisi, int yASaisi, int xBSaisi, int yBSaisi) {
        super(xASaisi, yASaisi, xBSaisi, yBSaisi);
        CalcullL();
    }
    //Déclaration de la méthode pour calculer la longueur et la largeur
    public void CalcullL(){
        if(xB > xA) l = xB-xA; else l = xA -xB;
        if(yB > yA) L = yB-yA; else L = yA -yB;
    }

    //Déclaration de la méthode Affiche
    @Override
    public void Affiche() {
        System.out.println("La longueur de votre figure est"+l+" et la largeur est "+L);
    }
    @Override
    public void Doubler_Perimetre() {
        super.Doubler_Perimetre();
        CalcullL();
    }
}
//Declaration de la classe Carre
class Carre extends FigureBis{
    //Déclaration des attributs
    int cote;
    //Declaration des constructeurs
    public Carre(int xASaisi, int yASaisi, int xBSaisi, int yBSaisi) {
        super(xASaisi, yASaisi, xBSaisi, yBSaisi);
        CalculCote();
    }
    private void CalculCote(){
        if((xB==xA)&&(yA==yB)) cote = xB-xA; else cote = 0 ;
    }
    @Override
    public void Affiche() {
        System.out.println("La valeur du côté de votre figure vaut "+cote);
    }
    @Override
    public void Doubler_Perimetre() {
        super.Doubler_Perimetre();
        CalculCote();
    }
}
//Déclaration de la classe Cercle
class Cercle extends Figure{
    double rayon;
    int xC, yC;
    //Déclaration du constructeur

    public Cercle(int xCSaisi, int yCSaisi, double RSaisi) {
        xC = xCSaisi;
        yC = yCSaisi;
        rayon = RSaisi;
    }
    //Déclaration des méthodes
    @Override
    public void Calcul_Perimetre() {
        double p;
        p = 2*3.1416*rayon;
        System.out.println("Le périmètre de votre cercle est "+p);
    }

    @Override
    public void Calcul_Surface() {
        double s;
        s = 3.1416*rayon*rayon;
        System.out.println("La surface de votre cercle est "+s);
    }

    @Override
    public void Doubler_Perimetre() {
        rayon=rayon*2;

    }

    @Override
    public void Affiche() {
        System.out.println("Le rayon du cercle vaut "+rayon);
    }
}
class Figure2{
    public static void main (String ARGS[]){
        Rectangle R1 = new Rectangle(10,15,50,40);
        R1.Affiche();
        R1.CalcullL();
    }
} t